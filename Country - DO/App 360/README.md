# Customer 360º View
The application will allow agents to search for clients (manually or automatically) and view registration and transactional data, like a list of recent orders, credit limits and so on. The data sources used in the application will be Zendesk Sunshine Custom Objects, populated through custom integration with the Segment platform.
### Manual search
When the app is launched it will identify the Organization present in the ticket and try to perform an automatic search based on the external_id attribute of the organization, which will contain the internal ABI client code. If no results are found, the application will allow manual search, as detailed below.

The app will display search elements to allow the support agent to select a search criteria and enter a value to search by that criteria. Each criteria item corresponds to an identifier available on the Zendesk Sunshine Custom Object for each Organization. Selecting the Search button will perform an API request to the Zendesk Sunshine Custom Objects API using the value entered by the agent to match on the identifier value of the poc and using the identifier type as selected by the search filter option that was chosen. Any results from the API request will be displayed in a list format in a results area of the app. Only the top 3 results from the search will be displayed. The list will be sorted alphabetically by organization name and each entry will include the organization name and the RNC value of the organization. There will be a button on each entry in the list to allow the support agent to select the organization to view more information. If only one result is found, the list will not be displayed and the agent will be taken directly to the contact information page.

| English |      Spanish      |
| ------- | :---------------: |
| POC ID  | Código de cliente |
| Tax ID  |   RNC / Cédula    |


### Contact Information
When an organization has been selected for display, the app will load a new screen to display more detailed information. The app will present this detailed information about the organization in a tabbed interface. The first tab will be the default view and it will display contact information for the organization, based on the POC's Sunshine Custom Object.
There will be a visual indicator to show the support agent if the organization is Non-digital.
The first section of contact information will display the following items: POC ID, POC Name, Owner’s Name, Tax ID, Phone(s), Email
The next section of contact information will display the following items: Region, DDC, Room, Route, BDR, VDI.
The next section will display the address information. The address will be formatted with the following information:

* Street
* City, State
* Postal code

If any piece of information does not have a value it will be displayed with a blank in the app interface.
None of the information that is displayed can be selected or modified, it will be read-only.
The moment the company information is found and presented to the agent, the application will also fill in ticket fields that will be mapped in the application's settings. Here is an example of a configuration in which the **Dirección** field (with ID 360040602874) is filled with the **Address** attribute found in the API:
```json
{ 
    "360040602874": "address" 
}
```
### Legal and marketing data

The second tab of the organization’s detailed information will display Legal and Marketing information, based on the POC's Sunshine Profile. None of the information that is displayed on this tab can be selected or modified, it is read-only.

The Legal section will display the following information: Visit Day, Delivery Date, Last Visit Date, Current Segmentation, Current attention model, Client Potential.

The Marketing section will display the following information: Client Program, Balance of Points, Total Generated Points, History of Used Points, Last Points Change Date, Client Registered in Program?.

The Digital Adoption section will display the following information: Last Login Date, Marketplace Products Coverage, B2B Share, Power User/Lapsed User.

### Order history

The third tab of the Organization’s detailed information will display Order History information. The app will display an option to specify a date range. The supported date range options will be:

* Last 30 days (select by default)
* Last 90 days
* Open date range
  * Start date
  * End date
  
The app will make a request to the Zendesk Sunshine Custom Objects API to retrieve orders for the last 30 days for the organization. If the user selects a different date range, another request may be made to retrieve more events to meet the criteria.

For each event returned in the request, the app will display them as a list of orders. The list will display up to 3 orders on a single page, additional orders in the list will be paginated and available to view with a page selector at the bottom of the list. The list will be ordered by most recent order to oldest order.

Each order displayed in the results list will show the following information: Order #, Order Status, Order Date, Order Total, NCF.

Each order in the list can be selected to view more details about the order. The details will be displayed as an overlay window that can be closed to view the order history list again.

The order details screen will display the following information: Order #, Order Status, Order Date, Delivery Date, Paid with, NCF, Tax Total, Order Total, and Products List.

The Products List of the order details will be organized into a table displaying the following information for each product in the order: Quantity, Product Name + SKU + Price, Total.


### Financial data
The fourth tab of the Organization’s detailed information will display Financial information based on the POC's Sunshine Profile. The Financial section will include the following information: Credit Limit, Available Credit, Credit Balance, Overdue Credit, Payment Term, Payment Method. None of the information that is displayed can be selected or modified, it will be read-only.

The next section will display the total of their last order.

### Order taking

The app will allow the agent to open a new window (a modal dialog) while working on a ticket. Inside this new frame there will be an external page, built and hosted by AB InBev, where the order taking system will be loaded.

The URL of this page must be added as an application setting. This setting can be updated by a Zendesk Support administrator and will have this default value: 

https://test-conv-micerveceria.abi-sandbox.net

The moment the new window is opened, the following parameters will be sent dynamically:


| Attribute name     |         Zendesk placeholder         |     Example      |
| ------------------ | :---------------------------------: | :--------------: |
| zendesk_ticket_id  |            {{ticket.id}}            |       111        |
| zendesk_agent_id   |         {{current_user.id}}         |   408602720754   |
| zendesk_enduser_id |       {{ticket.requester.id}}       |   411202306194   |
| poc_id             | {{ticket.organization.external_id}} |    0000230798    |
| user_id            |  {{ticket.requester.external_id}}   | 0000230798_19374 |

### Manage app permissions

The app will allow administrators to control which permissions will be given to each agent role. The permission management screen will be displayed when the administrator clicks the "Configure settings" button.

When the administrator clicks the "Save settings" button on this screen, the list of permissions will be saved in the application settings with the identified "role_permissions". To avoid configuration errors, these settings will not be displayed to administrators in the native app management screens of Zendesk Support.

Here's an example of the hidden settings:

```json
{
  "app_id": 1234,
  "product": "support",
  "settings": {
    "title": "Customer 360 View",
    "role_permissions": {
      "search_clients": "360009503553,360009503573,360009503593",
      "view_customer_data": "360009503573",
      "view_legal_data": "360009503573",
      "view_financial_data": "360009503573",
      "view_order_history": "360009503573",
      "take_orders": "360009503573"
    }
  }
}
```

There will be no detailed permission management for Zendesk Support administrators. They can perform any operation within the application.

### Sunshine API documentation

The following section will describe the API endpoints that will be consumed by the application.

It is expected that the following data structure exists in Sunshine.

* Object type = **order**
* Relationship type = **organization_orders** (1:Many)


### Object type: Order

##### Order Modified track call

In the Orders tab the agent will be able to see the order history for a given client. These will be sent to Zendesk Sunshine from Segment based on the "Order Modified" event types.

The agent is also able to select some predefined date ranges or custom dates, so the start_time and end_time filters should be used as search criteria.

Below is a sample of the order object that Segment will be sending to the Sunshine API:
```
PATCH /api/sunshine/objects/records
```

```json
{
    "data": {
        "type": "order",
        "external_id": "BB00361155",
        "attributes": {
            "poc_id": "0000230338",
            "currency": "DOP",
            "delivery_date": "06/11/2020",
            "discount": 0,
            "order_id": "BB00361155",
            "points_earned": 0,
            "products": [
                {
                    "brand": "Presidente Light",
                    "category": "",
                    "currency": "DOP",
                    "name": "Presidente Light 22 Onz Grande - Huacal [16 botellas] (Mediana)",
                    "original_price": "",
                    "original_quantity": "",
                    "packaging": "22.0OZ ",
                    "points_earned": "",
                    "position": 0,
                    "price": 1593.54,
                    "product_id": "-B000060",
                    "quantity": 7,
                    "recommendation_id": "",
                    "recommendation_type": "",
                    "recommended_quantity": "",
                    "sku": "-B000060",
                    "url": "",
                    "variant": ""
                },
                {
                    "brand": "The One",
                    "category": "",
                    "currency": "DOP",
                    "name": "The One 22 Onz Grande - Huacal [16 Unidades] (Mediana)",
                    "original_price": "",
                    "original_quantity": "",
                    "packaging": "22.0OZ ",
                    "points_earned": "",
                    "position": 3,
                    "price": 1348.5,
                    "product_id": "-B000082",
                    "quantity": 4,
                    "recommendation_id": "",
                    "recommendation_type": "",
                    "recommended_quantity": "",
                    "sku": "-B000082",
                    "url": "",
                    "variant": ""
                }
            ],
            "revenue": 20355.25,
            "shipping": 0,
            "tax": 11368.93,
            "total": 31724.18
        }
    }
}

```

The application will search for order type objects related to existing Organizations (POCs), using the List Related Object Records endpoint to obtain the list of orders.

Here's an example:

```
GET /api/sunshine/objects/records/zen:organization:370430694973/related/organization_orders
```


##### Relationship type: organization_orders (1:Many)
For the application to be able to find the list of orders from a POC, it is necessary to create a relationship between them.

Zendesk Sunshine will link the order object to the native organization object using the Create Relationship Record endpoint. 

Below is a sample of the relationship that Segment will be creating in the Sunshine API:

```
POST /api/sunshine/relationships/records
```

```json
{
    "data": {
        "relationship_type": "organization_orders",
        "source": "zen:organization:370430694973",
        "target": "5a526472-aa61-11ea-8bc3-75fc9ab6d468"
   }
}
```

### Supported Languages
Supported languages are defined in the dictionary JSON file.


### Default settings

#### rolePermissions
```json
    name: name_of_tab (to hide/display),
    groups: [role_id_A, role_id_B, role_id_C, ...]
```
```json
{
  "rolePermissions": [
    {
      "name": "View Customer Data",
      "groups": [
        1496522
      ]
    },
    {
      "name": "View Legal Data",
      "groups": [
        4338425,
        1496522
      ]
    }
  ]
 ```
 #### ticketFormFields
 ```json
    ticket_field_id: api_attribute
 ```
 Example
 ```json
  "ticketFormFields": {
    "360040256133": "order_id",
    "360042559554": "created_at",
    "360040602874": "address",
    "360041018934": "sku",
    "360040493353": "NO DATA",
    "360040493653": "NO DATA",
    "360042507133": "NO DATA",
    "360040494373": "NO DATA",
    "360042639474": "NO DATA",
    "360040602914": "available_credit",
    "360040603074": "NO DATA",
    "360042601313": "NO DATA"
  }
}
```