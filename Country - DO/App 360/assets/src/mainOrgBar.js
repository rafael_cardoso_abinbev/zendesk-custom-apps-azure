import SearchResultOrg from './components/SearchResult/SearchResultOrg.js';
import ZDClient from './services/ZDClient.js';
import { setters, methods } from './store/store.js';

document.addEventListener('DOMContentLoaded', () => {
  const initVueApp = (data) => {
    methods.getCurrentUserRoledId();
    ZDClient.getCurrentUserLocale()
      .then((userLocale) => {
        setters.setLocale(userLocale['currentUser.locale']);
        new Vue({
          el: '#app',
          render: h => h(SearchResultOrg),
        });
      });
  };
  ZDClient.init();
  ZDClient.events['ON_APP_REGISTERED'](initVueApp);
});