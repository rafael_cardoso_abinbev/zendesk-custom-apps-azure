let CLIENT = null;
let APP_SETTINGS = null;

import { state, getters, methods, setters } from '../store/store.js';
const ZDClient = {

  events: {
    ON_APP_REGISTERED(cb) {
      return CLIENT.on('app.registered', async (data) => {
        APP_SETTINGS = data.metadata.settings;
        APP_SETTINGS['installationId'] = data.metadata.installationId;
        APP_SETTINGS['appId'] = data.metadata.appId;
        if (data.context.location !== 'organization_sidebar') CLIENT.invoke('ticketFields:requester.hide');
        const appSettings =  await methods.getAppsInstall();
        let app360View = [];
        app360View = appSettings.installations.filter((element) =>element.id === ZDClient.app.settings.installationId);
        
        if(app360View.length === 0){
          app360View = appSettings.installations.filter((element) => element.settings.microserviceGrantType === ZDClient.app.settings.microserviceGrantType);
        }

        state.acessToken = app360View[0].settings.microserviceToken;
        await methods.verifyAcessToken(app360View);
        
        return cb(data);
      });
    },
    ON_API_NOTIFICATION(cb) {
      return CLIENT.on('api_notification.updateSettings', (data) => {
        APP_SETTINGS = data.body.settings;
        APP_SETTINGS['roleUpdated'] = data.body.roleUpdated;
        return cb(data.body.settings.rolePermissions);
      });
    },
    ON_REQUESTER_ID_CHANGED(cb) {
      return CLIENT.on('ticket.requester.id.changed', (data) => {
        return cb(data);
      });
    },
    ON_TICKET_FORM_CHANGED(cb) {
      return CLIENT.on('ticket.form.id.changed', (data) => {
        return cb(data);
      });
    },
  },

  init() {
    CLIENT = ZAFClient.init();
  },

  /**
   * Set getters for privite objects
   */
  app: {
    get settings() { return APP_SETTINGS; },

    /**
     * It returns true if the app is installed in the instance, false if
     * it's running locally
     */
    get isProduction() { return !!this.settings['IS_PRODUCTION']; }
  },
  /**
   * When ticket requester changes return the organizations of the new requester.
   * @return {Promise null || Object}
   */
  async hasOrganization() {
    return new Promise((resolve) => {
      setTimeout(() => {
        CLIENT.get('ticket.organization').then((requesterOrganizations) => {
          const res = requesterOrganizations.hasOwnProperty('ticket.organization')
            ? requesterOrganizations['ticket.organization']
            : null;
          resolve(res);
        });
      }, 500);
    });
  },

  async getCurrentUserRole() {
    return await CLIENT.get('currentUser.role');
  },
  getCurrentUserLocale: function() {
    return CLIENT.get('currentUser.locale');
  },
  async getCurrentAccountTimeZone() {
    return await CLIENT.get('currentAccount.timeZone');
  },

  /**
   * @returns {Promise}
   */
  async request(data) {
    return await CLIENT.request(data);
  },
  /**
   * @param {Int} frameSize
   * @param {Function} callback
   */
  /*   async openModal() {
      const modalInstance = await CLIENT.invoke('instances.create', {
        location: 'modal',
        url: this.app.settings['ordersUrl'],
        size: {
          width: '80vw',
          height: '80vh',
        },
      });
      const clientModal = CLIENT.instance(
        modalInstance['instances.create'][0].instanceGuid,
      );
    }, */

  async getCurrentFormFields() {
    return await CLIENT.get('ticketFields');
  },
  async setTicketRequester(requesterDetails) {
    return await CLIENT.set('ticket.requester', requesterDetails);
  },
  async getTicketRequester() {
    return await CLIENT.get('ticket.requester');
  },
  async getTicketId() {
    return await CLIENT.get('ticket.id');
  },
  async currentUserDetails() {
    return await CLIENT.get('currentUser');
  },
  async getCurrentLocation() {
    return await CLIENT._context.location;
  },
  async getTicketFieldValue(fieldId) {
    return await CLIENT.get('ticket.customField:custom_field_' + fieldId);
  },
  setTicketFieldValue(fieldId, fieldValue) {
    CLIENT.set('ticket.customField:custom_field_' + fieldId, fieldValue);
  },
  
  async getChangeTicket() {
    CLIENT.on('ticket.submit.start', function(data) {
      state.isChangeStatusTicket = true;
    });
    return await state.isChangeStatusTicket;
  },

  
  /**
   * It sets the frame height using on the passed value.
   * If no value has been passed, 80 will be set as default heigth.
   * @param {Int} newHeight
   */
  resizeFrame(appHeight) {
    CLIENT.invoke('resize', { width: '100%', height: `${appHeight}px` });
  },
  showNotification(message) {
    CLIENT.invoke('notify', message, 'notice');
  },

  /****** ORG LOCATION ******/

  async getOrganizationDetails() {
    return await CLIENT.get('organization.externalId');
  },

  hideFields() {
    let organizationFields = JSON.parse(APP_SETTINGS.orgFields);
    organizationFields.forEach((field) => {
      CLIENT.invoke('organizationFields:' + field + '.hide');
    });
  }
};

export default ZDClient;
