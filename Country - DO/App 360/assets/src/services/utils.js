export default {
  formatDate(dateToFormat, toRequest) {
    const newDate = new Date(dateToFormat);
    return toRequest
      ? newDate.getFullYear() + '-' + ('0' + (newDate.getMonth() + 1)).slice(-2) + '-' + ('0' + (newDate.getDate())).slice(-2)
      : ('0' + (newDate.getDate())).slice(-2) + '/' + ('0' + (newDate.getMonth() + 1)).slice(-2) + '/' + newDate.getFullYear();

  },
  calculateDateRange(days) {
    return new Date(new Date().setDate(new Date().getDate() - days));
  },
  removeGMTAndDefaultHour(dateToFormat) {
    return dateToFormat.toString().padStart(2, 0);
  },
};