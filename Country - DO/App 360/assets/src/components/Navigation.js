const template = /*html*/`
  <div class="settings">
    <button
      class="c-btn c-btn--anchor c-btn-settings c-btn-settings__left"
      v-if="resultsTab && getUserProfile !== null"
      @click="displaySearchResults">
      {{ i18n['backToSearchResult'] }}</button>
    <button
      v-if="searchTab || getIsDisplayingSearchResults"
      class="c-btn c-btn--anchor c-btn-settings c-btn-settings__left"
      @click="displaySearchForm">
      {{ i18n['backToSearchForm'] }}</button>
    <button
      v-if="settingsTab && getCurrentUserRoleId === 'admin'"
      class="c-btn c-btn--anchor c-btn-settings c-btn-settings__right"
      @click="displaySettings">
      {{ i18n['configureSettings'] }}</button>
  </div>
`;
import { getters, state, setters } from '../store/store.js';
import ZDClient from '../services/ZDClient.js';

export default {
  template: template,
  data() {
    return {
      searchTab: false,
      resultsTab: false,
      settingsTab: false
    }
  },
  computed: {
    state: () => state,
    ...getters
  },
  methods: {
    setTabs(searchTab, resultsTab, settingsTab) {
      this.searchTab = searchTab;
      this.resultsTab = resultsTab;
      this.settingsTab = settingsTab;
    },
    displaySettings() {
      setters.setIsDisplayingSettings();
      this.setTabs(true, true, false);
    },
    displaySearchForm() {
      state.isDisplayingSettings = false;
      state.isDisplayingSearchResults = false;
      this.setTabs(false, true, true);
    },
    displaySearchResults() {
      if (getters.getIsDisplayingSearchResults() === false) setters.setIsDisplayingSearchResults();
      state.isDisplayingSettings = false;
      this.setTabs(true, false, true);
    },
  },
  created() {
    if (getters.getIsDisplayingSearchResults()) {
      this.searchTab = true;
      this.settingsTab = true;
    } else {
      this.settingsTab = true;
    }
  },
  mounted() {
    ZDClient.resizeFrame(this.$root.$el.scrollHeight);
  },
  updated() {
    ZDClient.resizeFrame(this.$root.$el.scrollHeight);
  }
}