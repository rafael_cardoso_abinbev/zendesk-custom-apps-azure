const template = /*html*/`
<PulseLoader
  class="u-p-lg"
  color="#1f73b7"
  v-if="getIsSavingSettings">
</PulseLoader>
<div
  class="settings__form"
  v-else>
  <fieldset
  class="u-mb-sm u-position-relative u-mt"
  :style="isMenuOpen ? 'z-index: 1;' : ''">
    <div class="c-txt">
      <button
        class="c-txt__input c-txt__input--select"
        :class="{ 'is-open': isMenuOpen }"
        @click="isMenuOpen = !isMenuOpen"
        id="select-0"
        ><span dir="ltr">
          {{ optionSelected !== -1
            ? getAccountRoles[optionSelected].name
            : i18n['selectARole'] }}<span></span></span></button>
    </div>
    <div class="menu-container u-1/1">
      <ul
        :aria-hidden="isMenuOpen ? false : true"
        class="c-menu c-menu--down u-1/1 c-menu__role-list"
        :class="{ 'is-open': isMenuOpen }"
        @click="isMenuOpen = !isMenuOpen"
        :style="isMenuOpen ? '' : 'display: none;'">
        <li
          v-for="(role, index) in getAccountRoles"
          :key="role.id"
          class="c-menu__item"
          :class="{ 'is-checked': index === optionSelected }"
          @click="setSelectedRole(index, role.id)">{{ role.name }}</li>
      </ul>
    </div>
  </fieldset>
  <div
    class="settings__options u-mv-sm"
    v-if="optionSelected !== -1">
    <ul>
      <li class="u-mb-sm"
      v-for="(permission, index) in getRolesPermissions">
        <div class="c-chk">
          <input
            class="c-chk__input"
            :id="'permission' + index"
            type="checkbox"
            :checked="permission.groups.indexOf(getRoleId) !== -1"
            @click="setPermissionsToEditId(index)">
          <label
            class="c-chk__label c-chk__label--toggle"
            dir="ltr"
            :for="'permission' + index">{{ permission.name }}</label>
        </div>
      </li>
    </ul>
  </div>
  <div class="settings__button">
    <button
      class="c-btn inline-block c-btn--primary"
      :class="{ 'is-disabled': getRolesPermissions.length === 0 }"
      :disabled="getRolesPermissions.length === 0"
      @click="editPermissions">{{ i18n['save'] }}</button>
  </div>
</div>
`;
import { getters, methods, state, setters } from '../store/store.js';
import ZDClient from '../services/ZDClient.js';

export default {
  template: template,
  components: {
    PulseLoader: VueSpinner.PulseLoader
  },
  data() {
    return {
      isMenuOpen: false,
      optionSelected: -1,
      selectedRoleId: 0,
      temporalPermissions: []
    }
  },
  computed: {
    state: () => state,
    ...getters
  },
  methods: {
    setRolesSettings() {
      setters.setRolePermissions(ZDClient.app.settings.rolePermissions);
      methods.getAccountRoles()
        .then((customRoles) => {
          setters.setAccountRoles(customRoles.custom_roles);
        });
    },
    togglePermission(index) {
      methods.togglePermission(index);
    },
    setSelectedRole(index, selectedRoleId) {
      this.optionSelected = index;
      setters.setSelectedSettingRoleId(selectedRoleId);
    },
    setPermissionsToEditId(index) {
      methods.editRolePermission(index, state.selectedRoleId);
    },
    async editPermissions() {
      setters.setIsSavingSettings();
      const rolePermissions = await methods.editRolesPermissions();
      if (rolePermissions) {
        ZDClient.showNotification(getters.i18n()['appPermissionsSuccess'].message);
      } else {
        setters.setErrorMessage('appPermissionsError');
        setters.setIsError();
      }
      setters.setIsSavingSettings();
    }
  },
  created() {
    this.setRolesSettings();
  },
  mounted() {
    ZDClient.resizeFrame(this.$root.$el.scrollHeight);
  },
  updated() {
    setTimeout(() => {
      ZDClient.resizeFrame(this.$root.$el.scrollHeight);
    }, 100);
  }
}