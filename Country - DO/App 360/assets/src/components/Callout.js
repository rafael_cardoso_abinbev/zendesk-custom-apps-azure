const template = `
<div class="c-callout c-callout--error">
  <button
    class="c-callout__close"
    @click="closeCallout"
    v-if="!getCallFromOrgApp"></button>
  <strong class="c-callout__title"><span dir="ltr">{{ i18n[getErrorMessage].title }}</span></strong>
  <p class="c-callout__paragraph">{{ i18n[getErrorMessage].message }}</p>
</div>`;

import { getters, state, setters } from '../store/store.js';

export default {
  template,
  computed: {
    state: () => state,
    ...getters
  },
  methods: {
    closeCallout() {
      setters.setIsError();
    }
  }
};