const template = `
  <SearchForm></SearchForm>
  <Callout v-if="getIsError"></Callout>
`;

import ZDClient from '../../services/ZDClient.js';
import SearchForm from './SearchForm.js';
import { getters } from '../../store/store.js';
import Callout from '../Callout.js';

const Search = {
  components: {
    SearchForm,
    Callout
  },
  computed: {
    ...getters
  },
  template,
  mounted() {
    ZDClient.resizeFrame(this.$root.$el.scrollHeight);
  },
  updated() {
    ZDClient.resizeFrame(this.$root.$el.scrollHeight);
  },
};

export default Search;
