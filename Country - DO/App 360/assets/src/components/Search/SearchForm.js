const template = `
<div class="search-form">
  <fieldset
  class="u-mb-lg u-position-relative"
  :style="isMenuOpen ? 'z-index: 1;' : ''">
  <label class="c-txt__label">{{ i18n['searchClient'] }}</label>
    <div class="c-txt">
      <button
        class="c-txt__input c-txt__input--select"
        :class="{ 'is-open': isMenuOpen }"
        @click="isMenuOpen = !isMenuOpen"
        id="select-0"
        ><span dir="ltr">{{ i18n['searchDropdownOptions'][optionSelected] }}<span></span></span></button>
    </div>
    <div class="menu-container u-1/1">
      <ul
        :aria-hidden="isMenuOpen ? false : true"
        class="c-menu c-menu--down u-1/1"
        :class="{ 'is-open': isMenuOpen }"
        @click="isMenuOpen = !isMenuOpen"
        :style="isMenuOpen ? '' : 'display: none;'">
        <li
          class="c-menu__item"
          v-for="(option, index) in i18n['searchDropdownOptions']"
          @click="optionSelected = index"
          :class="{ 'is-checked': index === optionSelected }">
          {{ option }}
        </li>
      </ul>
    </div>
  </fieldset>
  <div class="c-txt" id="search-box">
    <div class="c-txt__input c-txt__input--media u-position-relative" id="txt-type-search-box">
      <input
        class="c-txt__input c-txt__input--bare"
        id="txt-type-search"
        type="search"
        :placeholder="i18n['search']"
        v-model="searchString">
      <div class="c-txt__input--media__figure">
        <svg xmlns="https://www.w3.org/2000/svg" width="16" height="16" focusable="false" viewBox="0 0 16 16">
          <circle cx="6" cy="6" r="5.5" fill="none" stroke="currentColor"/>
          <path stroke="currentColor" stroke-linecap="round" d="M15 15l-5-5"/>
        </svg>
      </div>
    </div>
  </div>
  <button
    class="c-btn c-btn--primary u-2/24 u-mt-xs search-form__button"
    :class="{ 'is-disabled': searchString === '' }"
    :disabled="searchString === ''"
    @click="searchProfile">{{ i18n['search'] }}</button>
  <Callout v-if="getIsError"></Callout>
  <OrganizationUsers v-if="getOrganizationUsersLoaded"></OrganizationUsers>
</div>
`;

import ZDClient from '../../services/ZDClient.js';
import { methods, getters, setters } from '../../store/store.js';
import Callout from '../Callout.js';
import OrganizationUsers from './OrganizationUsers.js';

const SearchForm = {
  template,
  components: {
    Callout,
    OrganizationUsers
  },
  data() {
    return {
      optionSelected: 0,
      isMenuOpen: false,
      searchString: ''
    }
  },
  computed: {
    ...getters
  },
  methods: {
    async searchProfile() {
      setters.setLoader();
      methods.getOrganizationAndOrders({
        id: this.searchString,
        searchBy: this.optionSelected === 0 ? 'customerId' : 'taxId'
      });
    },
    //rever porque estavam utilizando isto, não estão funcionando pra todas external ids
    parseSearchString() {
      return this.optionSelected === 0 && this.searchString.slice(0, 4) !== '0000'
        ? '0000' + this.searchString
        : this.searchString;
    }
  },

  mounted() {
    ZDClient.resizeFrame(this.$root.$el.scrollHeight);
  },
  updated() {
    ZDClient.resizeFrame(this.$root.$el.scrollHeight);
  }
};

export default SearchForm;
