const template = /*html*/ `
  <div v-if="getIsCreatingNewUser"">
    <NewUser></NewUser>
  </div>
  <div
    class="organization-users-list"
    v-else>

    <div class="c-callout c-callout--success" v-if="getIsNewCreateOrgByMs">
      <button
        class="c-callout__close"
        @click="closeWarning">
      </button>
      <strong class="c-callout__title"><span dir="ltr">{{ getOrgMs.accountId }} - {{ getOrgMs.name }}</span></strong>
      <p class="c-callout__paragraph">{{ i18n[getMessageCreateOrg].message }}</p>
    </div>
    
    <div v-if="!getIsErrorConsultAccount">
      <label class="c-txt__label">{{ i18n['organizationUsers'] }}</label>
        <ul>
          <li
          class="c-order u-mb-xs"
          v-for="(user, index) in usersToDisplay">
            <div class="c-order__info">
              <svg class="c-icon" viewBox="0 0 16 16">
                <path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                d="M4.5 2.5H2a.5.5 0 0 0-.5.5v12a.5.5 0 0 0 .5.5h12a.5.5 0 0 0 .5-.5V3a.5.5 0 0 0-.5-.5h-2.5m-7 8h2v2h-2zm0-4h2v2h-2zm4.5 1h3m-3 4h3m-7.464-7c-.3-2.08 1.332-4 3.45-4s3.749 1.92 3.449 4h-6.9z"></path>
              </svg>
              <div>
              <p class="c-info"><b>{{ getOrgMs.name}}</p>
                <p class="c-info"><b>{{ user.name  }}</p>
                <p class="c-info">{{ user.email  }}</p>
                <p class="c-info">{{ i18n['phone'] }}: {{ user.phone || '' }}</p>
              </div>
            </div>
            <button
              class="c-btn c-btn-view inline-block"
              @click="setAsRequester(index)">
              Select
            </button>
          </li>
        </ul>
        
        <p v-if="usersToDisplay.length == 0" class="c-callout--recessed u-p-xs">
          {{ i18n['noUsersInThisOrganization'] }}
        </p>

        <Pagination
          v-if="getOrganizationUsers.length > 5"
          @setRecordsToDisplay="setRecordsToDisplay"></Pagination>
        <div
          class="create-new-user__button">
          <button
            class="c-btn c-btn--primary organization-users-list__create-new-user"
            @click="createNewUser">{{ i18n['createNewUser'] }}</button>
        </div>

      <div>
  </div>
`;

import { getters, setters, state } from '../../store/store.js';
import ZDClient from '../../services/ZDClient.js';
import Pagination from '../Pagination.js';
import NewUser from './NewUser.js';


const OrganizationUsers = {
  template: template,
  components: {
    Pagination,
    NewUser
  },
  data() {
    return {
      usersToDisplay: [],
      perPage: 5
    }
  },
  filters: {
    capitalizeFirstLetter: string => string.charAt(0).toUpperCase() + string.slice(1),
    truncateString: (source) => {
      return source && source.length > 25 ? source.slice(0, 25 - 1) + "…" : source;
    }
  },
  computed: {
    state: () => state,
    ...getters
  },
  methods: {
    closeWarning(){
      setters.setIsNewCreateOrgByMs();
    },
    setOrganizationUsersList() {
      if (getters.getOrganizationUsers().length > 5) {
        setters.setPagesToDisplay(getters.getOrganizationUsers().length);
        setters.setCurrentPage(1);
      }
      this.setRecordsToDisplay(0);
    },
    setRecordsToDisplay(index) {
      this.usersToDisplay = this.paginate(getters.getOrganizationUsers());
    },
    paginate(orders) {
      let page = getters.getCurrentPage();
      let perPage = this.perPage;
      let from = (page * perPage) - perPage;
      let to = (page * perPage);
      return orders.slice(from, to);
    },
    createNewUser() {
      if (getters.getIsError()) setters.setIsError();
      setters.setIsCreatingNewUser();
    },
    async setAsRequester(index) {
      let ticketRequester = await ZDClient.getTicketRequester();
      if (ticketRequester.hasOwnProperty('ticket.requester') && ticketRequester['ticket.requester'].id === this.usersToDisplay[index].id) {
        setters.setIsDisplayingSearchResults();
      } else {
        setters.setIsOrganizationAvailable();
        const newRequester = await ZDClient.setTicketRequester({
          id: this.usersToDisplay[index].id
        });
        if (newRequester) {
          setters.setRequesterDetails({
            email: this.usersToDisplay[index].email,
            phone: this.usersToDisplay[index].phone
          });
        }
      }
    }
  },
  created() {
    this.setOrganizationUsersList();
  },
  mounted() {
    ZDClient.resizeFrame(this.$root.$el.scrollHeight);
  },
  updated() {
    ZDClient.resizeFrame(this.$root.$el.scrollHeight);
  },
  beforeDestroy() {
    if (getters.getIsCreatingNewUser()) setters.setIsCreatingNewUser();
  }
}

export default OrganizationUsers;