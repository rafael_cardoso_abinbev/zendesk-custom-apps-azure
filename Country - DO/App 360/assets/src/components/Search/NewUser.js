const template = `
<PulseLoader
  class="u-p-lg"
  color="#1f73b7"
  v-if="getIsCreating">
</PulseLoader>
<div
  class="new-user-form"
  v-else>
  <div class="new-user-form__header">
    <label class="c-txt__label">{{ i18n['newUserForm'] }}</label>
    <button
      @click="closeCreateNewUser"
      class="c-btn c-btn--sm c-btn--primary c-btn--primary c-btn-close">
      <svg class="c-icon" viewBox="0 0 16 16" id="zd-svg-icon-16-x-stroke">
        <path stroke="currentColor" stroke-linecap="round" d="M3 13L13 3m0 10L3 3"></path>
      </svg>
    </button>
  </div>
  <input
    class="c-txt__input new-user-form_inputs"
    v-model="name"
    :placeholder="i18n['newUserName']" type="text">
  <input
    class="c-txt__input new-user-form_inputs"
    v-model="phone"
    :placeholder="i18n['newUserPhone']"
    type="text">
  <p
    v-if="phoneError"
    class="form-validation-error">{{ i18n['userPhoneError'] }}</p>
  <input
    class="c-txt__input new-user-form_inputs"
    v-model="email"
    :placeholder="i18n['email']"
    type="text">
  <p
    v-if="emailError"
    class="form-validation-error">{{ i18n['userEmailError'] }}</p>
  <button
    class="c-btn c-btn--primary new-user-form_inputs"
    :disabled="name === ''"
    @click="createNewUser">{{ i18n['newUserSaveButton'] }}</button>
</div>`;

import { methods, getters, setters } from '../../store/store.js';
import ZDClient from '../../services/ZDClient.js';

const NewUser = {
  template,
  components: {
    PulseLoader: VueSpinner.PulseLoader
  },
  data() {
    return {
      name: '',
      email: '',
      phone: '',
      emailError: false,
      phoneError: false
    }
  },
  watch: {
    email() {
      let regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      this.emailError = this.email === ''
        ? false
        : regex.test(this.email)
          ? false
          : true
    },
    phone() {
      let regex = /^\++?[1-9][0-9]\d{6,14}$/
      this.phoneError = this.phone === ''
        ? false
        : regex.test(this.phone)
          ? false
          : true
    }
  },
  computed: {
    ...getters
  },
  methods: {
    async createNewUser() {
      setters.setIsCreating();
      const newUser = await methods.createNewUser({
        user: {
          name: this.name,
          email: this.email,
          phone: this.phone,
          organization_id: getters.getTemporalOrganizationId(),
          verified: true
        }
      });
      if (newUser) {
        setters.setIsOrganizationAvailable();
        ZDClient.showNotification(getters.i18n()['userCreationSuccess']);
        ZDClient.setTicketRequester({
          id: newUser.user.id
        });
        setters.setMultipleOrganizationUsers(getters.getTemporalOrganizationId());
        this.closeCreateNewUser();
        setters.setIsCreating();
        setters.setRequesterDetails({
          email: this.email,
          phone: this.phone
        });


      } else {
        setters.setIsCreating();
        setters.setErrorMessage('userCreationFail');
        setters.setIsError();
      }
    },
    closeCreateNewUser() {
      setters.setIsCreatingNewUser();
    }
  },
  mounted() {
    ZDClient.resizeFrame(this.$root.$el.scrollHeight);
  },
  updated() {
    ZDClient.resizeFrame(this.$root.$el.scrollHeight);
  }
};

export default NewUser;