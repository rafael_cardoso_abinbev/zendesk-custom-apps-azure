const template = `
<div>
  <ul class="c-pagination">

    <li
      :style="getCurrentPage > 1 ? 'visibility: visible;' : 'visibility: hidden;'"
      class="c-pagination__page "
      @click="setCurrentPage(1)">
        <img src="./img/fast-forward-lw.svg"/>
    </li>

    <li
    :style="getCurrentPage > 1 ? 'visibility: visible;' : 'visibility: hidden;'"
    class="c-pagination__page c-pagination__page--previous"
    @click="setCurrentPagePrevious(getCurrentPage-2)">previous
    </li>

      <li
        class="c-pagination__page"
        :class="{ 'is-current': (p) === getCurrentPage }"
        v-for="(p, index) in setPages.slice(getStartPagination, getFinishPagination)"
        @click="setCurrentPage(p)">
        {{ p }}
      </li>



    <li
    :style="getCurrentPage < setPages.length ? 'visibility: visible;' : 'visibility: hidden;'"
    class="c-pagination__page c-pagination__page--next"
    @click="setCurrentPageNext(getCurrentPage++)">next</li>

    <li
      :style="getCurrentPage == setPages.length ? 'visibility: hidden;' : 'visibility: visible;'"
      class="c-pagination__page"
      @click="setCurrentPage(setPages.length)">
      <img src="./img/fast-forward-rw.svg"/>
    </li>

  </ul>
</div>`;

import { getters, state, setters } from '../store/store.js';

const Pagination = {
  template,
  data() {
    return {
      page: 1,
      perPage: 5,
      ac : 1,
      maxValue : 0,
      minValue: 1,
    }
  },
  computed: {
    state: () => state,
    ...getters,
    setPages() {
      let numberOfPages = Math.ceil(getters.getPagesToDisplay() / this.perPage);
      this.maxValue = numberOfPages;
      return this.setNumberOfPages(numberOfPages);
    }
  },

  methods: {
    setCurrentPage(page) {
      setters.setSlicesPages(page-1);
      setters.setCurrentPage(page);
      this.$emit('setRecordsToDisplay', page-1);
    },
    setCurrentPagePrevious(index) {
      setters.setSlicesPages(index);
      setters.setCurrentPage(index + 1);
      this.$emit('setRecordsToDisplay', index);
    },
    setCurrentPageNext(index) {
      setters.setSlicesPages(index);
      setters.setCurrentPage(index + 1);
      this.$emit('setRecordsToDisplay', index);
    },
    setNumberOfPages(numberOfPages) {
      let pages = [];
      for (let index = 1; index <= numberOfPages; index++) {
        pages.push(index);
      }
      return pages;
    },
    minMaxHandler(ac){
      if(ac < this.minValue){
        this.ac = this.minValue;
      }
      if(ac > this.maxValue){
        this.ac =  this.maxValue;
      }
    },

  }
};

export default Pagination;