const template = `
<PulseLoader
  class="u-p-lg"
  color="#1f73b7"
  v-if="getIsLoading">
</PulseLoader>
<div
  class="c-main"
  v-else>
  <Navigation></Navigation>
  <AppSettings v-if="getIsDisplayingSettings"></AppSettings>
  <Search v-else-if="!getIsDisplayingSearchResults"></Search>
  <SearchResult v-else-if="getIsDisplayingSearchResults"></SearchResult>
</div>`;

import ZDClient from '../services/ZDClient.js';
import { state, getters, methods, setters } from '../store/store.js';
import Navigation from '../components/Navigation.js';
import Search from '../components/Search/Search.js';
import SearchResult from '../components/SearchResult/SearchResult.js';
import AppSettings from '../components/AppSettings.js';

const App = {
  template,
  components: {
    Navigation,
    AppSettings,
    Search,
    SearchResult,
    PulseLoader: VueSpinner.PulseLoader
  },
  computed: {
    state: () => state,
    ...getters
  },
  methods: {
    ...methods,
    displaySettings() {
      setters.setIsDisplayingSettings();
    },
    displaySearchForm() {
      setters.setIsDisplayingSearchResults();
      state.isDisplayingSettings = false;
    }
  },
  created() {
    methods.getCurrentUserRoledId();
    ZDClient.getTicketRequester()
      .then((ticketRequester) => {
        if (ticketRequester.hasOwnProperty('ticket.requester')) {
          methods.requesterEmailChanged();
        }
      });
  },
  mounted() {
    ZDClient.resizeFrame(this.$el.scrollHeight);
  },
  updated() {
    ZDClient.resizeFrame(this.$el.scrollHeight);
  },
};

export default App;