const template = /*html*/ `
<div>
  <h2 class="c-content__title">{{ i18n['financialData'] }}</h2>
  <section class="c-section">
    <p class="c-content__p"><b>{{ i18n['creditLimit'] }}</b><span class="c-line"></span><span>RD$ {{ getOrgMs.credit == undefined ? 0 : getOrgMs.credit.total | twoDecimals }}</span></p>
    <p class="c-content__p"><b>{{ i18n['usedCredit'] }}</b><span class="c-line"></span><span>RD$ {{ getOrgMs.credit == undefined ? 0 : getOrgMs.credit.balance | twoDecimals }}</span></p>
    <p class="c-content__p"><b>{{ i18n['availableCredit'] }}</b><span class="c-line"></span><span>RD$ {{ getOrgMs.credit == undefined ? 0 : getOrgMs.credit.available || 0 | twoDecimals }}</span></p>
    <p class="c-content__p"><b>{{ i18n['paymentTerm'] }}</b><span class="c-line"></span><span>{{ getOrgMs.credit == undefined ? '' : getOrgMs.credit.paymentTerms || '' }}</span></p>
    <p class="c-content__p"><b>{{ i18n['paymentMethod'] }}</b><span class="c-line"></span><span>{{ getOrgMs.credit == undefined ? '' : getOrgMs.paymentMethods || '' }}</span></p>
  </section>
  <section class="c-section green-border">
    <p class="c-content__p"><b>{{ i18n['lastOrder'] }}</b><span class="c-line"></span>
      <span>RD$ {{ getUserOrdersMs.length > 0 ? getUserOrdersMs[0].total : 0 || 0 | twoDecimals }}</span>
    </p>
  </section>
</div>
`;

import { getters } from '../../store/store.js';

export default {
  template: template,
  computed: {
    ...getters
  },
  filters: {
    twoDecimals: (amount) => {
      return (amount/1).toFixed(2);
    }
  },
  updated() {
    this.$emit('updated');
  },
}