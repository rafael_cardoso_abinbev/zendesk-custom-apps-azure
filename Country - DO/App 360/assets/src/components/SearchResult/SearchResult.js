const template = `
<div>
  <NavigationBar @setTabIndex="setTabIndex"></NavigationBar>
  <ContactInformation v-if="tabIndex === 0"></ContactInformation>
  <Legal v-if="tabIndex === 1"></Legal>
  <Financial v-if="tabIndex === 2"></Financial>
  <Order v-if="tabIndex === 3"></Order>
  <TakeOrder v-if="tabIndex === 4"></TakeOrder>
</div>
`;

import ZDClient from '../../services/ZDClient.js';
import { state, getters, methods } from '../../store/store.js';
import NavigationBar from '../SearchResult/NavigationBar.js';
import ContactInformation from '../SearchResult/ContactInformation.js';
import Legal from '../SearchResult/Legal.js';
import Order from '../SearchResult/Order.js';
import Financial from '../SearchResult/Financial.js';
import TakeOrder from '../SearchResult/TakeOrder.js';

const App = {
  template,
  components: {
    NavigationBar,
    ContactInformation,
    Legal,
    Order,
    Financial,
    TakeOrder
  },
  data() {
    return {
      tabIndex: 0
    }
  },
  computed: {
    state: () => state,
    ...getters
  },
  methods: {
    ...methods,
    setTabIndex(tabIndex) {
      this.tabIndex = tabIndex;
    }
  },
  created() {
  },
  mounted() {
    ZDClient.resizeFrame(this.$root.$el.scrollHeight);
  },
  updated() {
    ZDClient.resizeFrame(this.$root.$el.scrollHeight);
  },
};

export default App;