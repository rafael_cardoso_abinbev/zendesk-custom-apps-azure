const template = /*html*/ `
<div v-if="!displayOrder">
  <h2 class="c-content__title u-mb-sm">{{ i18n['orders'] }}</h2>
  <fieldset
  class="u-mb-lg u-position-relative"
  :style="isMenuOpen ? 'z-index: 1;' : ''">
    <div class="c-txt u-mv-xs">
      <button
        class="c-txt__input c-txt__input--select"
        :class="{ 'is-open': isMenuOpen }"
        @click="isMenuOpen = !isMenuOpen"
        id="select-0"
        ><span dir="ltr">{{ i18n['searchOrdersOptions'][optionSelected] }}<span></span></span></button>
    </div>
    <div class="menu-container u-1/1">
      <ul
        :aria-hidden="isMenuOpen ? false : true"
        class="c-menu c-menu--down u-1/1"
        :class="{ 'is-open': isMenuOpen }"
        @click="isMenuOpen = !isMenuOpen"
        :style="isMenuOpen ? '' : 'display: none;'">
        <li
          class="c-menu__item"
          v-for="(option, index) in i18n['searchOrdersOptions']"
          @click="getEvents(index)"
          :class="{ 'is-checked': index === optionSelected }">
          {{ option }}
        </li>
      </ul>
    </div>
  </fieldset>
  <div class="l-grid" v-if="displayDateRange">
    <div class="l-grid__item">
      <div class="date-from">
        <label class="c-txt__label" for="txt-type-date">{{ i18n['from'] }}</label>
        <input
          class="c-txt__input c-txt__input--select"
          id="date-from"
          placeholder="date"
          type="date"
          v-model="startDate"
          @change="checkDate">
      </div>
      <div class="date-to">
        <label class="c-txt__label" for="txt-type-date">{{ i18n['to'] }}</label>
        <input
          class="c-txt__input c-txt__input--select"
          id="date-to"
          placeholder="date"
          type="date"
          v-model="endDate"
          @change="checkDate">
      </div>
    </div>
    <button
      class="c-btn c-btn--primary orders_serarch_button"
      @click="sendEventRequest">{{ i18n['search'] }}</button>
  </div>


    <div class="c-callout c-callout--error" v-if="getIsErrorConsultOrder">
      <button
        class="c-callout__close"
        @click="closeError"></button>
      <strong class="c-callout__title"><span dir="ltr">{{ i18n[getErrorMessage].title }}</span></strong>
      <p class="c-callout__paragraph">{{ i18n[getErrorMessage].message }}</p>
    </div>

    <PulseLoader
    class="u-p-lg"
    color="#1f73b7"
    v-if="getIsCreating">
  </PulseLoader>

  <Callout v-if="getIsError"></Callout>
  <div
    class="orders-list"
    v-if="ordersToDisplay.length > 0  && !getIsErrorConsultOrder && !getIsCreating">
    <ul>
      <li
      class="c-order u-mb-xs"
      v-for="(order, index) in ordersToDisplay">
        <div class="c-order__info">
          <svg class="c-icon" viewBox="0 0 16 16">
            <path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
            d="M4.5 2.5H2a.5.5 0 0 0-.5.5v12a.5.5 0 0 0 .5.5h12a.5.5 0 0 0 .5-.5V3a.5.5 0 0 0-.5-.5h-2.5m-7 8h2v2h-2zm0-4h2v2h-2zm4.5 1h3m-3 4h3m-7.464-7c-.3-2.08 1.332-4 3.45-4s3.749 1.92 3.449 4h-6.9z"></path>
          </svg>
          <div>
            <p class="c-info u-mb-xs">
              <b>{{ order.orderNumber | truncateString }}</b>
              <span
                v-if="order.status"
                class="c-tag c-tag--pill u-ml-xs u-border"
                :style="statusClass(order.status)">{{ order.status == undefined ? '' : order.status | capitalizeFirstLetter }}</span>
            </p>
            <p class="c-info u-mb-xs">{{ i18n['orderedOn'] }} {{ order.placementDate | timeFormatted }}</p>
            <p class="c-info u-mb-xs">{{ i18n['updatedOn'] }} {{ order.updatedAt | timeFormatted }}</p>
            <p class="c-info u-mb-xs">NCF </p>
            <p class="c-info"><b>RD$ {{ order.total || '' | twoDecimals }}</b>
            </p>
          </div>
        </div>
        <button
          class="c-btn c-btn-view inline-block"
          @click="displayOrderDetails(index)">
          <svg class="c-icon" viewBox="0 0 16 16">
            <g fill="currentColor">
              <path d="M15.83 7.42C15.1 6.38 12.38 3 8 3S.9 6.38.17 7.42a.99.99 0 0 0 0 1.16C.9 9.62 3.62 13 8 13s7.1-3.38 7.83-4.42a.99.99 0 0 0 0-1.16zM8 11c-1.65 0-3-1.35-3-3s1.35-3 3-3 3 1.35 3 3-1.35 3-3 3z"></path>
              <circle cx="8" cy="8" r="2"></circle>
            </g>
          </svg>
        </button>
      </li>
    </ul>
  </div>
  <Pagination
    v-if="getUserOrdersMs.length > 3 && !getIsErrorConsultOrder && !getIsCreating"
    @setRecordsToDisplay="setRecordsToDisplay">
  </Pagination>
</div>
<div v-else>
  <OrderDetail
    @closeOrderDetails="displayOrderDetails"
    :currentOrder="currentOrder"></OrderDetail>
</div>
`;

import { getters, methods, state, setters } from '../../store/store.js';
import utils from '../../services/utils.js';
import ZDClient from '../../services/ZDClient.js';
import Callout from '../Callout.js';
import Pagination from '../Pagination.js';
import OrderDetail from '../SearchResult/OrderDetail.js';

const Order = {
  template: template,
  components: {
    Callout,
    Pagination,
    OrderDetail,
    PulseLoader: VueSpinner.PulseLoader,
  },
  data() {
    return {
      isMenuOpen: false,
      displayDateRange: false,
      isSearching: false,
      startDate: "",
      endDate: "",
      optionSelected: 0,
      ordersToDisplay: [],
      displayOrder: false,
      currentPage: 1,
      perPage: 5,
    };
  },
  computed: {
    state: () => state,
    ...getters,
  },
  filters: {
    capitalizeFirstLetter: (string) =>
      string.charAt(0).toUpperCase() + string.slice(1),
    timeFormatted: (orderDate) => {
      return utils.formatDate(orderDate, false);
    },
    truncateString: (source) => {
      return source && source.length > 25
        ? source.slice(0, 25 - 1) + "…"
        : source;
    },
    twoDecimals: (amount) => {
      return (amount / 1).toFixed(2);
    },
  },
  methods: {
    closeError() {
      setters.setIsErrorConsultOrder();
    },
    displayOrderDetails(orderId) {
      this.currentOrder = this.ordersToDisplay[orderId];
      this.displayOrder = !this.displayOrder;
    },
    getEvents(dateRangeIndex) {
      this.optionSelected = dateRangeIndex;
      if (dateRangeIndex === 2) {
        this.setCurrentDate();
        if (getters.getIsError()) setters.setIsError();
        this.displayDateRange = true;
      } else {
        this.startDate = "";
        this.endDate = "";
        this.sendEventRequest(dateRangeIndex);
        this.displayDateRange = false;
      }
    },
    setRecordsToDisplay() {
      this.ordersToDisplay = this.paginate(getters.getUserOrdersMs());
    },
    paginate(orders) {
      let page = getters.getCurrentPage();
      let perPage = this.perPage;
      let from = page * perPage - perPage;
      let to = page * perPage;
      return orders.slice(from, to);
    },
    async sendEventRequest(dateRangeIndex) {
      setters.setIsCreating();
      state.startPagination = 0;
      state.finishPagination = 5;
      if (getters.getIsErrorConsultOrder()) setters.setIsErrorConsultOrder();
      this.isSearching != this.isSearching;
      setters.setCurrentPage(1);

      let OrdersMs = [];

      let startDateToSearch = this.startDate
        ? this.startDate + " 00:00:00.00"
        : utils.formatDate(
            utils.calculateDateRange(dateRangeIndex === 0 ? 30 : 90),
            true
          ) + " 00:00:00.00";
      let endtDateToSearch = this.endDate
        ? this.endDate + " 23:59:59.00"
        : utils.formatDate(new Date(), true) + " 23:59:59.00";
      let endDate30DaysOr90Days = this.startDate
        ? this.startDate + " 00:00:00.00"
        : utils.formatDate(new Date(), true) + " 00:00:00.00";

      if (dateRangeIndex === 0) {
        OrdersMs = state.userOrderMsInMemory.filter(
          (element) =>
            new Date(element.placementDate) >=
            new Date(
              new Date(endDate30DaysOr90Days).setDate(new Date().getDate() - 30)
            )
        );
      } else if (dateRangeIndex === 1) {
        OrdersMs = state.userOrderMsInMemory.filter(
          (element) =>
            new Date(element.placementDate) >=
            new Date(
              new Date(endDate30DaysOr90Days).setDate(new Date().getDate() - 90)
            )
        );
      } else {
        OrdersMs = state.userOrderMsInMemory.filter(
          (element) =>
            new Date(element.placementDate) >= new Date(startDateToSearch) &&
            new Date(element.placementDate) <= new Date(endtDateToSearch)
        );
      }

      if (OrdersMs.length === 0) {
        setters.setIsErrorConsultOrder();
        setters.setErrorMessage("ordersNotFound");
      } else {
        setters.setOrdersMs(OrdersMs);
        setters.setPagesToDisplay(getters.getUserOrdersMs().length);
        setters.setCurrentPage(1);
        this.setRecordsToDisplay();
      }
      setters.setIsCreating();
    },

    checkDate() {
      if (this.startDate && this.endDate) {
        if (new Date(this.startDate) > new Date(this.endDate)) {
          if (getters.getIsErrorConsultOrder())
            setters.setIsErrorConsultOrder();
          setters.setIsErrorConsultOrder();
          setters.setErrorMessage("startDateBiggerEndDate");
        }
      }
    },
    statusClass: (status) => {
      if (status === "CONFIRMED" || status === "PLACED") {
        return "color: white; background-color: #7ea132";
      }
      if (status === "CANCELLED" || status === "DENIED") {
        return "color: white; background-color: #EF6C6F";
      }
      if (status === "DELIVERED") {
        return "color: white; background-color: #2C7DE8";
      }
      if (status === "MODIFIED") {
        return "color: white; background-color: #c6b70d";
      }
      if (status === "PENDING") {
        return "color: white; background-color: #2A3949";
      }
    },
    setCurrentDate() {
      this.startDate = utils.formatDate(new Date(), true);
      this.endDate = utils.formatDate(new Date(), true);
    },
  },
  created() {
    if (getters.getIsError()) setters.setIsError();
    this.getEvents(0);
  },
  mounted() {
    ZDClient.resizeFrame(this.$root.$el.scrollHeight);
  },
  updated() {
    ZDClient.resizeFrame(this.$root.$el.scrollHeight);
  },
  destroyed() {
    if (getters.getIsError()) setters.setIsError();
  },
};

export default Order;