const template = /*html*/ `
<div>
  <div class="c-links-container">
    <h2 class="c-content__title">{{ i18n['oderDetails'] }}</h2>
    <button
      @click="closeOrderDetails"
      class="c-btn c-btn--sm c-btn--primary c-btn--primary c-btn-close">
      <svg class="c-icon" viewBox="0 0 16 16" id="zd-svg-icon-16-x-stroke">
        <path stroke="currentColor" stroke-linecap="round" d="M3 13L13 3m0 10L3 3"></path>
      </svg>
    </button>
  </div>
  <section class="c-section">
    <p class="c-content__p">
      <b>{{ currentOrder.orderNumber || '' }}</b>
      <span
        v-if="currentOrder.status"
        class="c-tag c-tag--pill u-ml-xs u-border"
        :style="statusClass(currentOrder.status)">{{ currentOrder.status == undefined ? 'PENDING' : currentOrder.status | capitalizeFirstLetter }}</span>
    </p>
    <p class="c-content__p">
      {{ i18n['orderedOn'] }} <span class="c-line"></span> {{ currentOrder.placementDate || '' | timeFormatted }}
    </p>
    <p class="c-content__p">
      {{ i18n['updatedOn'] }} <span class="c-line"></span> {{ currentOrder.updatedAt || '' | timeFormatted }}
    </p>
    <p class="c-content__p">
      {{ i18n['deliveryDate'] }} <span class="c-line"></span> {{ currentOrder.delivery == undefined ? '' : currentOrder.delivery.date | timeFormatted }}
    </p>
    <p class="c-content__p">
      {{ i18n['paidWith'] }} <span class="c-line"></span> {{ currentOrder.paymentMethod || '' }}
    </p>
    <p class="c-content__p">
      {{ i18n['ncf'] }} <span class="c-line"></span> 
    </p>
    <p class="c-content__p">
      {{ i18n['invoiceStatus'] }} <span class="c-line"></span> 
    </p>
    <table class="c-table u-mb-xs u-mt-xs">
      <caption class="u-display-none">{{ i18n['productList'] }}</caption>
      <thead>
        <tr class="c-table__row c-table__row--header">
          <th class="c-table__row__cell c-table__row__cell--truncate">{{ i18n['qty'] }}</th>
          <th class="c-table__row__cell c-table__row__cell--truncate" colspan="2">{{ i18n['product'] }}</th>
          <th class="c-table__row__cell c-table__row__cell--truncate">{{ i18n['total'] }}</th>
        </tr>
      </thead>
      <tbody>
        <tr class="c-table__row" v-for="product in currentOrder.items" v-if="product !== null">
          <td class="c-table__row__cell">{{ product.quantity || 0 }}</td>
          <td class="c-table__row__cell" colspan="2">
            <span class="c-txt__hint">{{ i18n['sku'] }}: {{ product.sku || '' }}</span><br/>
            {{ product.name || '' }}<br/>
            <span class="c-txt__hint">RD$ {{ product.price || 0 | twoDecimals }}</span>
            <span>(Promotion)</span>
          </td>
          <td
            class="c-table__row__cell"
            v-if="product.quantity > 0">RD$ {{ product.quantity * product.price || 0 | twoDecimals }}</td>
          <td
            class="c-table__row__cell"
            v-else>RD$ 0</td>
        </tr>
      </tbody>
    </table>
    <p class="c-content__p">
        {{ i18n['total'] }} <span class="c-line"></span> <b>RD$ {{ currentOrder.total || 0 | twoDecimals }}</b>
    </p>
    <p class="c-content__p">
      {{ i18n['discount'] }} <span class="c-line"></span> <b>RD$ {{ currentOrder.discount || 0 | twoDecimals }}</b>
    </p>
    <p class="c-content__p">
        {{ i18n['taxes'] }} <span class="c-line"></span> <b>RD$ {{ currentOrder.tax || 0 | twoDecimals}}</b>
    </p>
  </section>
</div>
`;

import { getters } from '../../store/store.js';
import utils from '../../services/utils.js';

export default {
  template: template,
  props: {
    currentOrder: Object
  },
  computed: {
    ...getters
  },
  filters: {
    capitalizeFirstLetter: string => string.charAt(0).toUpperCase() + string.slice(1),
    timeFormatted: (orderDate) => {
      return orderDate !== undefined
        ? utils.formatDate(orderDate, false)
        : '';
    },
    twoDecimals: (amount) => {
      return (amount/1).toFixed(2);
    }
  },
  methods: {
    statusClass: status => {
      if (status === "CONFIRMED" || status === "PLACED") {
        return "color: white; background-color: #7ea132";
      }
      if (status === "CANCELLED" || status === "DENIED") {
        return "color: white; background-color: #EF6C6F";
      }
      if (status === "DELIVERED") {
        return "color: white; background-color: #2C7DE8";
      }
      if (status === "MODIFIED") {
        return "color: white; background-color: #c6b70d";
      }
      if (status === "PENDING") {
        return "color: white; background-color: #2A3949";
      }
    },
    closeOrderDetails() {
      this.$emit('closeOrderDetails');
    }
  },
  created() {
  },
}