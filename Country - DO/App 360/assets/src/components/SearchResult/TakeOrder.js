const template = /*html*/`
<div class="take-orders">
  <h2 class="c-content__title">{{ i18n['orderTitle'] }}</h2>
  <p class="take-order__message">{{ i18n['orderSentence'] }}</p>
  <a
    class="c-btn"
    :href="getUrlFromSettings"
    target="_blank"
    role="button">{{ i18n['takeOrder'] }}</a>
</div>
`;

import ZDClient from '../../services/ZDClient.js';
import { getters, methods } from '../../store/store.js';

const Legal = {
  template,
  methods: {

  },
  computed: {
    ...getters
  },
  created() {
    methods.setOrdersUrl();
  },
  mounted() {
    ZDClient.resizeFrame(this.$root.$el.scrollHeight);
  },
  updated() {
    ZDClient.resizeFrame(this.$root.$el.scrollHeight);
  },
};

export default Legal;