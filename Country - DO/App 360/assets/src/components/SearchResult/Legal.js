const template = /*html*/`
    <div>
        <h2 class="c-content__title">{{ i18n['legal'] }}</h2>

        <section class="c-section">
            <p class="c-content__p"><b>{{ i18n['currentSegmentation'] }}</b>
              <span class="c-line"></span><span>{{ getOrgMs.subSegment || '' }}</span>
            </p>
            <p class="c-content__p"><b>{{ i18n['clientPotential'] }}</b>
              <span class="c-line"></span><span>{{ getOrgMs.potential || '' }}</span>
            </p>
        </section>

        <h2 class="c-content__title u-mt-sm">{{ i18n['marketing'] }}</h2>
        <section class="c-section">

            <p class="c-content__p"><b>{{ i18n['registeredInProgram'] }}</b>
              <span class="c-line"></span>
              <span>{{ getMarketingMs.name ? i18n['yes'] : i18n['no'] }}</span>
            </p>

            <p class="c-content__p"><b>{{ i18n['clientProgram'] }}</b>
              <span class="c-line"></span><span>{{ getMarketingMs.name || '' }}</span>
            </p>

            <p class="c-content__p"><b>{{ i18n['balanceOfPoints'] }}</b>
              <span class="c-line"></span><span>{{ getMarketingMs.initialBalance  == undefined ? 0 : getMarketingMs.initialBalance}}</span>
            </p>

        </section>
    </div>
`;

import ZDClient from '../../services/ZDClient.js';
import { state, getters } from '../../store/store.js';

const Legal = {
  template,
  computed: {
    state: () => state,
    ...getters
  },
  filters: {
    twoDecimals: (amount) => {
      return (amount/1).toFixed(2);
    }
  },
  mounted() {
    ZDClient.resizeFrame(this.$root.$el.scrollHeight);
  },
  updated() {
    ZDClient.resizeFrame(this.$root.$el.scrollHeight);
  },
};

export default Legal;