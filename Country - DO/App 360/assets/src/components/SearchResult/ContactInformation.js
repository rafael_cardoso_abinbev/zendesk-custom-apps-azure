const template = `
<div class="contact-information">
  <p class="c-content__p">
    <b>{{ i18n['customerInformation'] }}</b>
    <span
    class="c-tag c-tag--pill u-ml-xs digital-pill c-pill-right c-pill-custom"
    :style="digitalClass(getOrgMs.accountId)">{{ getOrgMs.accountId ? i18n['digital'] : i18n['nonDigital'] | capitalizeFirstLetter }}</span>
  </p>
  <section class="c-section">

    <p class="c-content__p"><b><span>{{ i18n['searchDropdownOptions'][0] }}</span></b>
      <span class="c-line"></span>
      <span>{{ getOrgMs.accountId || '' }}</span>
    </p>

    <p class="c-content__p"><b><span>{{ i18n['pocName'] }}</span></b>
      <span class="c-line"></span>
      <span>{{ getOrgMs.name || '' }}</span>
    </p>

    <p class="c-content__p"><b><span>{{ i18n['taxID'] }}</span></b>
      <span class="c-line"></span>
      <span>{{ getOrgMs.taxId }}</span>
    </p>

    <p class="c-content__p"><b>{{ i18n['phone'] }}</b><span class="c-line"></span>
      <span v-if="getRequesterDetails">{{ getRequesterDetails.phone }} </span>
      <span v-else>{{ getUserProfile.organization_fields.phone }} </span>
    </p>
    <p class="c-content__p" v-if="getRequesterDetails"><b>{{ i18n['email'] }}</b><span class="c-line"></span>
      <span v-if="getRequesterDetails">{{ getRequesterDetails.email | truncateString }} </span>
    </p>

  </section>
  <section class="c-section">

    <p class="c-content__p"><b>{{ i18n['region'] }}</b>
      <span class="c-line"></span>
      <span>{{ getOrgMs.deliveryRegion || '' }}</span>
    </p>
    
    <p class="c-content__p"><b>{{ i18n['ddc'] }}</b>
      <span class="c-line"></span>
      <span>{{ getOrgMs.deliveryCenterId }}</span>
    </p>
    
    <p class="c-content__p"><b>{{ i18n['room'] }}</b>
      <span class="c-line"></span>
      <span>{{ getOrgMs.erpSalesCenter || '' }}</span>
    </p>
    
    <p class="c-content__p"><b>{{ i18n['route'] }}</b>
      <span class="c-line"></span>
      <span>{{ getOrgMs.deliveryRoute || '' }}</span>
    </p>
    
    <p class="c-content__p"><b>{{ i18n['bdr'] }}</b>
      <span class="c-line"></span>
      <span>{{ getOrgMs.salesRepresentative.name || '' }}</span>
    </p>
  
  </section>
  <section class="c-section">
    <p class="c-content__p"><b>{{ i18n['address'] }}</b><span class="c-line"></span>
    <span>
      <p><span>{{ getOrgMs.deliveryAddress.address }}</span></p>
    </span>
    </p>
  </section>
</div>
`;

import { state, getters } from '../../store/store.js';

const ContactInformation = {
  template,
  computed: {
    state: () => state,
    ...getters
  },
  filters: {
    capitalizeFirstLetter: string => string.charAt(0).toUpperCase() + string.slice(1),
    truncateString: (source) => {
      return source && source.length > 25 ? source.slice(0, 25 - 1) + "…" : source;
    }
  },
  methods: {
    digitalClass: digital => {
      if (digital) {
        return 'color: rgb(31, 115, 183)' // Blue
      } else {
        return 'color: rgb(255, 0, 0)' // Red
      }
    }
  },
};

export default ContactInformation;
