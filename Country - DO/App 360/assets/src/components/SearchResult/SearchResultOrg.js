const template = `
  <PulseLoader
    class="u-p-lg"
    color="#1f73b7"
    v-if="getIsLoading">
  </PulseLoader>
  <div v-else-if="!getIsError"
  class="org-main-container">
    <NavigationBar @setTabIndex="setTabIndex"></NavigationBar>
    <ContactInformation v-if="tabIndex === 0"></ContactInformation>
    <Legal v-if="tabIndex === 1"></Legal>
    <Financial v-if="tabIndex === 2"></Financial>
    <Order v-if="tabIndex === 3"></Order>
    <TakeOrder v-if="tabIndex === 4"></TakeOrder>
  </div>
  <Callout
  v-else></Callout>
`;

import ZDClient from '../../services/ZDClient.js';
import utils from '../../services/utils.js';
import { state, getters, methods, setters } from '../../store/store.js';
import NavigationBar from '../SearchResult/NavigationBar.js';
import ContactInformation from '../SearchResult/ContactInformation.js';
import Legal from '../SearchResult/Legal.js';
import Order from '../SearchResult/Order.js';
import Financial from '../SearchResult/Financial.js';
import TakeOrder from '../SearchResult/TakeOrder.js';
import Callout from '../Callout.js';

const SearchResultOrg = {
  template,
  components: {
    NavigationBar,
    ContactInformation,
    Legal,
    Order,
    Financial,
    TakeOrder,
    PulseLoader: VueSpinner.PulseLoader,
    Callout
  },
  data() {
    return {
      tabIndex: 0
    }
  },
  computed: {
    state: () => state,
    ...getters
  },
  methods: {
    ...methods,
    setTabIndex(tabIndex) {
      this.tabIndex = tabIndex;
    },

    async setOrganizationToDisplay() {
      setters.setLoader();
      const orgExternalId = await ZDClient.getOrganizationDetails();
      const orgMsTest = await methods.getOrganizationByMsDetail(orgExternalId['organization.externalId']);
      setters.setOrganizationByMs(orgMsTest);

      const dataMarketingMs = await methods.getMarketingByMs(orgExternalId['organization.externalId']);
      if(dataMarketingMs != null){
        const dataProgramIdMs = await methods.getProgramIdByMs(dataMarketingMs.programId);
        setters.setMarketingByMs(dataProgramIdMs);
      }else{
        setters.setMarketingByMs([]);
      }
      
      if (orgExternalId['organization.externalId'] !== null) {
        const orgDetails = await this.getOrganizationDetails(orgExternalId['organization.externalId']);
        if (orgDetails) {

          const OrdersMs = await methods.getOrdersByMicroService(orgExternalId['organization.externalId']);
          OrdersMs[0].orders.sort(function (x, y) {
            let a = new Date(x.placementDate),
                b = new Date(y.placementDate);
            return b- a;
          });
          setters.setOrdersMs(OrdersMs[0].orders);
          setters.setOrderMsInMemory(OrdersMs[0].orders);
          setters.setProfile(orgDetails.organizations[0]);
          setters.setLoader();
        }
      } else {
        setters.setErrorMessage('notFound');
        setters.setCallFromOrgApp();
        setters.setIsError();
        setters.setLoader();
      }
    },
    async hideOrganizationFields() {
      if (getters.getCurrentUserRoleId() !== 'admin') ZDClient.hideFields();
    },
    async getOrganizationDetails(orgExternalId) {
      return await methods.getOrganizationByExternalId(orgExternalId);
    },
  },

  created() {
    this.setOrganizationToDisplay();
    this.hideOrganizationFields();
  },
  mounted() {
    ZDClient.resizeFrame(this.$root.$el.scrollHeight);
  },
  updated() {
    ZDClient.resizeFrame(this.$root.$el.scrollHeight);
  }
};

export default SearchResultOrg;