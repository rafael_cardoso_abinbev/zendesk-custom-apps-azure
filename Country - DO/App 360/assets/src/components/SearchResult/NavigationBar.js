const template = `
<nav class="u-mv">
  <ul class="c-tab__list" role="tablist">
    <li
      class="c-tab__list__item"
      :class="{ 'is-active': currentTabIndex === 0 }"
      v-if="getRolePermissions.indexOf(0) !== -1">
      <a
        href="#"
        :title="i18n['customerInformation']"
        @click="setTab(0)">
        <svg
          class="c-tab__icon"
          viewBox="0 0 16 16"
          id="zd-svg-icon-16-user-solo-fill">
          <g fill="currentColor">
            <circle cx="8" cy="5" r="4"></circle>
            <path d="M8 10a6 6 0 0 0-6 5.47.5.5 0 0 0 .5.53h10.97a.5.5 0 0 0 .5-.53A6 6 0 0 0 8 10z"></path>
          </g>
        </svg>
      </a>
    </li>
    <li
      class="c-tab__list__item"
      :class="{ 'is-active': currentTabIndex === 1 }"
      v-if="getRolePermissions.indexOf(1) !== -1">
      <a
        href="#"
        :title="i18n['legalAndMarketingData']"
        @click="setTab(1)">
        <svg class="c-tab__icon" viewBox="0 0 16 16" id="zd-svg-icon-16-book-open-fill">
          <path fill="currentColor" d="M15.2 1.2C11.81.51 8.84 1.8 8 2.22 7.16 1.8 4.19.51.8 1.2c-.46.09-.8.51-.8.99v11.7c0 .29.13.57.35.76.23.19.54.27.84.22 3.01-.54 5.64.61 6.37.97.14.07.29.1.44.1.15 0 .3-.03.44-.1.73-.36 3.36-1.51 6.37-.97.3.05.61-.03.84-.22.23-.19.35-.46.35-.76V2.19c0-.48-.34-.9-.8-.99zM3.01 3.75c1.09.02 2.19.2 3.29.53a.75.75 0 0 1-.22 1.47c-.07 0-.15-.01-.22-.03-.96-.3-1.93-.45-2.87-.47a.749.749 0 0 1-.74-.76c.01-.42.36-.72.76-.74zm0 3c1.09.02 2.19.2 3.29.53a.75.75 0 0 1-.22 1.47c-.07 0-.15-.01-.22-.03-.96-.3-1.93-.45-2.87-.47a.749.749 0 1 1 .02-1.5zm3.79 4.47a.76.76 0 0 1-.94.5c-.96-.29-1.93-.45-2.87-.47a.762.762 0 0 1-.74-.76c.01-.41.36-.74.76-.74 1.09.02 2.19.2 3.29.53.39.12.62.54.5.94zm6.29.03c-.95.02-1.91.17-2.87.47a.76.76 0 0 1-.94-.5c-.12-.4.1-.82.5-.94 1.09-.33 2.2-.51 3.28-.53h.01c.41 0 .74.33.75.74.02.41-.31.75-.73.76zm0-3c-.94.02-1.91.17-2.87.47a.76.76 0 0 1-.94-.5c-.12-.4.1-.81.5-.94 1.1-.33 2.2-.51 3.28-.53h.01c.41 0 .74.33.75.74.02.41-.31.75-.73.76zm0-3c-.94.02-1.91.17-2.87.47a.76.76 0 0 1-.94-.5c-.12-.4.1-.81.5-.94 1.1-.33 2.2-.51 3.28-.53h.01c.41 0 .74.33.75.74.02.41-.31.75-.73.76z"></path>
        </svg>
      </a>
    </li >
    <li
      class="c-tab__list__item"
      :class="{ 'is-active': currentTabIndex === 2 }"
      v-if="getRolePermissions.indexOf(2) !== -1">
      <a
        href="#"
        :title="i18n['financialData']"
        @click="setTab(2)">
        <svg class="c-tab__icon" viewBox="0 0 16 16" version="1.1">
          <g id="surface1">
            <path style="fill:none;stroke-width:40;stroke-linecap:butt;stroke-linejoin:miter;stroke:currentColor;stroke-opacity:1;stroke-miterlimit:4;" d="M 145.019531 312.011719 C 142.944336 380.981445 176.025391 411.987305 249.023438 413.94043 C 327.026367 415.039062 362.060547 380.004883 358.032227 312.988281 C 352.050781 255.004883 296.020508 239.990234 251.953125 234.008789 C 203.979492 217.041016 152.954102 208.984375 152.954102 139.038086 C 152.954102 90.942383 185.058594 60.058594 251.953125 61.035156 C 312.011719 61.035156 348.999023 86.05957 348.022461 145.019531 " transform="matrix(0.032,0,0,0.032,0,0)" />
            <path style="fill:none;stroke-width:30;stroke-linecap:butt;stroke-linejoin:miter;stroke:currentColor;stroke-opacity:1;stroke-miterlimit:4;" d="M 250 15.014648 L 250 484.985352 " transform="matrix(0.032,0,0,0.032,0,0)" />
          </g>
        </svg>
      </a>
    </li >
    <li
      class="c-tab__list__item"
      :class="{ 'is-active': currentTabIndex === 3 }"
      v-if="getRolePermissions.indexOf(3) !== -1">
      <a
        href="#"
        :title="i18n['orderData']"
        @click="setTab(3)">
        <svg class="c-tab__icon" viewBox="0 0 16 16" id="zd-svg-icon-16-clipboard-list-fill">
          <path fill="currentColor" d="M8 0C5.79 0 4 1.79 4 4v1h8V4c0-2.21-1.79-4-4-4zm6 2h-.5c-.28 0-.5.22-.5.5V5c0 .55-.45 1-1 1H4c-.55 0-1-.45-1-1V2.5c0-.28-.22-.5-.5-.5H2c-.55 0-1 .45-1 1v12c0 .55.45 1 1 1h12c.55 0 1-.45 1-1V3c0-.55-.45-1-1-1zM6.5 13.5c0 .28-.22.5-.5.5H4.5c-.28 0-.5-.22-.5-.5V12c0-.28.22-.5.5-.5H6c.28 0 .5.22.5.5v1.5zm0-4c0 .28-.22.5-.5.5H4.5c-.28 0-.5-.22-.5-.5V8c0-.28.22-.5.5-.5H6c.28 0 .5.22.5.5v1.5zM12 13H9c-.28 0-.5-.22-.5-.5s.22-.5.5-.5h3c.28 0 .5.22.5.5s-.22.5-.5.5zm0-4H9c-.28 0-.5-.22-.5-.5S8.72 8 9 8h3c.28 0 .5.22.5.5s-.22.5-.5.5z"></path>
        </svg>
      </a>
    </li >
    <li
      class="c-tab__list__item"
      :class="{ 'is-active': currentTabIndex === 4 }"
      v-if="getRolePermissions.indexOf(4) !== -1">
      <a
        href="#"
        :title="i18n['takeOrder']"
        @click="setTab(4)">
        <svg class="c-tab__icon" viewBox="0 0 16 16" version="1.1">
          <g id="surface1">
            <path style=" stroke:none;fill-rule:nonzero;fill:currentColor;fill-opacity:1;" d="M 13.199219 10.347656 C 12.449219 10.347656 11.839844 10.957031 11.839844 11.707031 C 11.839844 12.457031 12.449219 13.066406 13.199219 13.066406 C 13.949219 13.066406 14.558594 12.457031 14.558594 11.707031 C 14.558594 10.957031 13.949219 10.347656 13.199219 10.347656 Z M 13.199219 12.105469 C 12.980469 12.105469 12.800781 11.925781 12.800781 11.707031 C 12.800781 11.484375 12.980469 11.304688 13.199219 11.304688 C 13.421875 11.304688 13.601562 11.484375 13.601562 11.707031 C 13.601562 11.925781 13.421875 12.105469 13.199219 12.105469 Z M 13.199219 12.105469 " />
            <path style=" stroke:none;fill-rule:nonzero;fill:currentColor;fill-opacity:1;" d="M 13.070312 4.691406 L 10.71875 4.691406 C 10.542969 4.691406 10.398438 4.835938 10.398438 5.011719 L 10.398438 11.253906 C 10.398438 11.429688 10.542969 11.574219 10.71875 11.574219 L 11.058594 11.574219 C 11.21875 11.574219 11.359375 11.460938 11.378906 11.300781 C 11.503906 10.339844 12.273438 9.742188 13.199219 9.742188 C 14.128906 9.742188 14.894531 10.339844 15.023438 11.300781 C 15.042969 11.460938 15.183594 11.574219 15.339844 11.574219 L 15.679688 11.574219 C 15.855469 11.574219 16 11.429688 16 11.253906 L 16 8.132812 C 16 8.058594 15.972656 7.984375 15.925781 7.929688 L 13.316406 4.808594 C 13.253906 4.734375 13.164062 4.691406 13.070312 4.691406 Z M 11.039062 7.253906 L 11.039062 5.652344 C 11.039062 5.476562 11.183594 5.332031 11.359375 5.332031 L 12.769531 5.332031 C 12.863281 5.332031 12.953125 5.375 13.015625 5.449219 L 14.347656 7.046875 C 14.523438 7.257812 14.375 7.574219 14.101562 7.574219 L 11.359375 7.574219 C 11.183594 7.574219 11.039062 7.429688 11.039062 7.253906 Z M 11.039062 7.253906 " />
            <path style=" stroke:none;fill-rule:nonzero;fill:currentColor;fill-opacity:1;" d="M 0.320312 11.574219 L 1.140625 11.574219 C 1.296875 11.574219 1.4375 11.460938 1.457031 11.300781 C 1.585938 10.339844 2.351562 9.742188 3.28125 9.742188 C 4.207031 9.742188 4.976562 10.339844 5.101562 11.300781 C 5.121094 11.460938 5.261719 11.574219 5.421875 11.574219 L 9.441406 11.574219 C 9.617188 11.574219 9.761719 11.429688 9.761719 11.253906 L 9.761719 3.253906 C 9.761719 3.078125 9.617188 2.933594 9.441406 2.933594 L 0.320312 2.933594 C 0.144531 2.933594 0 3.078125 0 3.253906 L 0 11.253906 C 0 11.429688 0.144531 11.574219 0.320312 11.574219 Z M 0.320312 11.574219 " />
            <path style=" stroke:none;fill-rule:nonzero;fill:currentColor;fill-opacity:1;" d="M 3.28125 10.347656 C 2.53125 10.347656 1.921875 10.957031 1.921875 11.707031 C 1.921875 12.457031 2.53125 13.066406 3.28125 13.066406 C 4.03125 13.066406 4.640625 12.457031 4.640625 11.707031 C 4.640625 10.957031 4.03125 10.347656 3.28125 10.347656 Z M 3.28125 12.105469 C 3.058594 12.105469 2.878906 11.925781 2.878906 11.707031 C 2.878906 11.484375 3.058594 11.304688 3.28125 11.304688 C 3.5 11.304688 3.679688 11.484375 3.679688 11.707031 C 3.679688 11.925781 3.5 12.105469 3.28125 12.105469 Z M 3.28125 12.105469 " />
          </g>
        </svg>
      </a>
    </li >
  </ul >
</nav >
`;

import ZDClient from '../../services/ZDClient.js';
import { state, getters } from '../../store/store.js';

const NavigationBar = {
  template,
  data() {
    return {
      currentTabIndex: 1
    }
  },
  computed: {
    state: () => state,
    ...getters,
    getRolePermissions() {
      let tabsToDisplay = [];
      if (state.currentUserRoleId === 'admin') {
        tabsToDisplay = [0, 1, 2, 3, 4];
      } else {
        let currentRolePermissions = JSON.parse(ZDClient.app.settings.rolePermissions);
        tabsToDisplay = currentRolePermissions.map((role, index) => role.groups.indexOf(state.currentUserRoleId) !== -1
          ? index
          : undefined
        ).filter(x => typeof x !== "undefined");
      }
      this.setTab(tabsToDisplay[0]);
      return tabsToDisplay;
    }
  },
  methods: {
    setTab(tabIndex) {
      this.$emit('setTabIndex', tabIndex);
      this.currentTabIndex = tabIndex;
    }
  },
};

export default NavigationBar;
