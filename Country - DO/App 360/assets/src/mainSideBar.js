import App from './components/App.js';
import ZDClient from './services/ZDClient.js';
import { methods, setters } from './store/store.js';

document.addEventListener('DOMContentLoaded', () => {
  const initVueApp = (data) => {
    ZDClient.getCurrentUserLocale()
      .then((userLocale) => {
        setters.setLocale(userLocale['currentUser.locale']);
        new Vue({
          el: '#app',
          render: h => h(App),
        });
      });
  };
  
  
  ZDClient.init();
  ZDClient.events['ON_APP_REGISTERED'](initVueApp);
  ZDClient.events['ON_REQUESTER_ID_CHANGED'](methods.requesterEmailChanged);
  ZDClient.events['ON_TICKET_FORM_CHANGED'](methods.getTicketFormFields);
  ZDClient.events['ON_API_NOTIFICATION'](setters.setRolePermissions);
});