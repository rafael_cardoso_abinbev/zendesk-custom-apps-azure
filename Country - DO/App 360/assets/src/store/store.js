import i18n from '../i18n/dictionary.js';
import ZDClient from '../services/ZDClient.js';
import utils from '../services/utils.js';

export const state = Vue.observable({
  i18n,
  isLoading: false,
  isCreating: false,
  isSearchFormVisible: false,
  currentUserRoleId: 0,
  userProfile: null,
  userOrdersMs : [],
  userOrderMsInMemory: [],
  requesterDetails: null,
  organizationUsers: [],
  organizationUsersLoaded: false,
  selectedProfileIndex: 0,
  errorMessage: '',
  messageCreateOrg: '',
  isError: false,
  isErrorConsultOrder: false,
  isCreateOrgByMs: false,
  isDisplayingSettings: false,
  isDisplayingSearchResults: false,
  accountRoles: null,
  rolesPermissions: [],
  selectedRoleId: 0,
  ticketFormId: 0,
  currentPage: 1,
  pagesToDisplay: 0,
  isCreatingNewUser: false,
  temporalOrganizationId: 0,
  isOrganizationAvailable: false,
  callFromOrgApp: false,
  isSavingSettings: false,
  takeOrdersUrl: '',
  isTicket : false,
  startPagination: 0,
  finishPagination: 5,
  isChangeStatusTicket: false,
  acessToken: ''
});

export const getters = {
  i18n: () => state.i18n,
  getIsLoading: () => state.isLoading,
  getIsCreating: () => state.isCreating,
  getIsSearchFormVisible: () => state.isSearchFormVisible,
  getCurrentUserRoleId: () => state.currentUserRoleId,
  getUserProfile: () => state.userProfile,
  getUserOrdersMs:() => state.userOrdersMs,
  getRequesterDetails: () => state.requesterDetails,
  getOrganizationUsers: () => state.organizationUsers,
  getOrganizationUsersLoaded: () => state.organizationUsersLoaded,
  getErrorMessage: () => state.errorMessage,
  getMessageCreateOrg: () => state.messageCreateOrg,
  getIsError: () => state.isError,
  getIsErrorConsultOrder: () => state. isErrorConsultOrder,
  getIsNewCreateOrgByMs: () => state.isCreateOrgByMs,
  getIsDisplayingSettings: () => state.isDisplayingSettings,
  getIsDisplayingSearchResults: () => state.isDisplayingSearchResults,
  getAccountRoles: () => state.accountRoles,
  getRolesPermissions: () => state.rolesPermissions,
  getRoleId: () => state.selectedRoleId,
  getCurrentPage: () => state.currentPage,
  getPagesToDisplay: () => state.pagesToDisplay,
  getIsCreatingNewUser: () => state.isCreatingNewUser,
  getTemporalOrganizationId: () => state.temporalOrganizationId,
  getCallFromOrgApp: () => state.callFromOrgApp,
  getIsSavingSettings: () => state.isSavingSettings,
  getUrlFromSettings: () => state.takeOrdersUrl,
  getOrgMs: () => state.orgData,
  getMarketingMs: () => state.marketingMS,
  getIsErrorConsultAccount: () => state.isErrorConsultAccount,
  getStartPagination: () =>  state.startPagination,
  getFinishPagination: () =>  state.finishPagination
  
};

export const setters = {
  async setMultipleOrganizationUsers(organizationId) {
    setters.setUsersFromOrganizations([]);
    setters.setOrganizationUsersLoaded(false);
    const organizationUsers = await methods.getUsersFromOrganizations(organizationId);
    setters.setUsersFromOrganizations(organizationUsers.users);
    setters.setTemporalOrganizationId(organizationId);
    setters.setOrganizationUsersLoaded(true);
  },
  setIsErrorConsultAccount(){
    state.isErrorConsultAccount = !state.isErrorConsultAccount;
  },
  setOrganizationByMs(data){
    state.orgData = null;
    state.orgData = data[0];
  },
  setMarketingByMs(data){
    state.marketingMS = [];
    if(data === null){
      state.marketingMS = [];
    }else{
      state.marketingMS = data;
    }
  },
  setProfile(profile) {
    state.userProfile = null;
    state.userProfile = profile;
  },
  setOrdersMs(data){
    state.userOrdersMs = [];
    state.userOrdersMs = data;
  },

  setOrderMsInMemory(data){
    state.userOrderMsInMemory = [];
    state.userOrderMsInMemory = data;
  },

  setLocale(userLocale) {
    if (i18n[userLocale] === undefined) {
      let variation = userLocale.split('-');
      i18n[variation[0]] === undefined
        ? state.i18n = i18n['es']
        : state.i18n = i18n[variation[0]]
    } else {
      state.i18n = i18n[userLocale];
    }
  },
  setLoader() {
    state.isLoading = !state.isLoading;
  },
  setIsCreating() {
    state.isCreating = !state.isCreating;
  },
  setErrorMessage(errorMessage) {
    state.errorMessage = errorMessage;
  },
  setCreateNewOrgByMs(messageCreateOrg) {
    state.messageCreateOrg = messageCreateOrg;
  },

  setIsError() {
    state.isError = !state.isError;
  },

  setIsErrorConsultOrder() {
    state.isErrorConsultOrder = !state.isErrorConsultOrder;
  },
  setIsNewCreateOrgByMs() {
    state.isCreateOrgByMs = !state.isCreateOrgByMs;
  },
  
  setIsDisplayingSettings() {
    state.isDisplayingSettings = !state.isDisplayingSettings;
  },
  setIsDisplayingSearchResults() {
    state.isDisplayingSearchResults = !state.isDisplayingSearchResults;
    if(state.isTicket){
      state.isDisplayingSearchResults = true;
    }
  },
  setAccountRoles(roles) {
    state.accountRoles = roles;
  },
  setCurrentPage(page) {
    state.currentPage = page;
  },

  setSlicesPages(page) {
    if(page === 0|| page === 1 || page === 2 ){
      state.startPagination = 0;
      state.finishPagination = 5;
    }
    else{
      state.startPagination = page -2;
      state.finishPagination = state.startPagination + 5;   
    }
    const totalOfPage = Math.ceil(getters.getPagesToDisplay() / 5);
    if(page > (totalOfPage-1) ){
      state.startPagination = totalOfPage - 4;
      state.finishPagination = totalOfPage - 1;
    }


  },

  setIsCreatingNewUser() {
    state.isCreatingNewUser = !state.isCreatingNewUser;
  },
  setPagesToDisplay(pages) {
    state.pagesToDisplay = 0;
    state.pagesToDisplay = pages;
  },
  setUsersFromOrganizations(organizationUsers) {
    state.organizationUsers = organizationUsers.sort((a, b) => a.name.localeCompare(b.name));
  },
  setTemporalOrganizationId(temporalOrganizationId) {
    state.temporalOrganizationId = temporalOrganizationId;
  },
  setOrganizationUsersLoaded(organizationUsersLoaded) {
    state.organizationUsersLoaded = organizationUsersLoaded;
  },
  setIsOrganizationAvailable() {
    state.isOrganizationAvailable = !state.isOrganizationAvailable;
  },
  setRequesterDetails(requesterDetails) {
    state.requesterDetails = requesterDetails;
  },
  async setRolePermissions(rolePermissions) {
    state.rolesPermissions = [];
    state.rolesPermissions = JSON.parse(rolePermissions);
    state.currentUserRoleId = 0;
    state.currentUserRoleId = await ZDClient.getCurrentUserRole().then((roleId) => roleId['currentUser.role']);
    if (state.currentUserRoleId === ZDClient.app.settings.roleUpdated) {
      ZDClient.showNotification(state.i18n['roleSettingsChanged']);
    }
  },
  setSelectedSettingRoleId(roleId) {
    state.selectedRoleId = roleId;
  },
  setCallFromOrgApp() {
    state.callFromOrgApp = !state.callFromOrgApp;
  },
  setIsSavingSettings() {
    state.isSavingSettings = !state.isSavingSettings;
  },
  setTakeOrdersUrl(url) {
    state.takeOrdersUrl = url;
  }
}

export const methods = {
  async hasOrganization() {
    return await ZDClient.hasOrganization();
  },
  async getCurrentUserRoledId() {
    state.currentUserRoleId = 0;
    state.currentUserRoleId = await ZDClient.getCurrentUserRole().then((roleId) => roleId['currentUser.role']);
  },
  async requesterEmailChanged() {
    if (!state.isOrganizationAvailable) {
      setters.setLoader();
      state.isTicket = true;
      methods.getTicketOrganization()
        .then((response) => {
          if (!response) {
            methods.resetValuesAfterNotFound();
            if (getters.getIsLoading()) setters.setLoader();
          }
        });
    } else {
      setters.setIsDisplayingSearchResults();
      setters.setIsOrganizationAvailable();
    }
    let statusTicketChange = await ZDClient.getChangeTicket();
    if(state.isChangeStatusTicket){
      if (getters.getIsLoading()) setters.setLoader();  
    }
    
  },

  async getTicketOrganization() {

    const hasOrganization = await methods.hasOrganization();
    const requester = await ZDClient.getTicketRequester();

    
    if (hasOrganization !== null && hasOrganization.hasOwnProperty('externalId') && hasOrganization.externalId !== null) {
      if (!state.userProfile || (hasOrganization.id !== state.userProfile.id)) methods.getOrganizationAndOrders({
        id: hasOrganization.externalId,
        searchBy: 'customerId'
      });
      return true;
    }
    
    if (hasOrganization !== null && hasOrganization.id !== null &&  !hasOrganization.hasOwnProperty('externalId')) {
      const orgData = await methods.getDataFromOrganizations(hasOrganization.id);
      if (!state.userProfile || (hasOrganization.id !== state.userProfile.id)) methods.getOrganizationAndOrders({
        id: orgData.organization.external_id,
        searchBy: 'customerId'
      });
      return true;
    } 
    
    else {
      state.userProfile = null;
      return false;
    }
  },
  //Here to get datas from External Id to populate 360 in ticket
  async getOrganizationByMsDetail(orgExternalId) {
    return await methods.getOrganizationByMicroService(orgExternalId);
  },

  async getOrganizationAndOrders(searchAttributes) {

    if (state.isError === true) {
      setters.setIsError();    
    }
      
    const currentOrganization = await methods.getOrganizationByExternalId(searchAttributes.id);

    if (currentOrganization.count > 0) {

      const ticketRequester = await ZDClient.getTicketRequester();
      if (ticketRequester.hasOwnProperty('ticket.requester')) {
        methods.checkSameOrganization(ticketRequester['ticket.requester'].organizations, currentOrganization.organizations[0].id);
        methods.getRequesterEmailAndPhone(ticketRequester['ticket.requester'].identities);
      }

      setters.setMultipleOrganizationUsers(currentOrganization.organizations[0].id);
      state.currentUserRoleId = await ZDClient.getCurrentUserRole().then((roleId) => roleId['currentUser.role']);
      setters.setProfile(currentOrganization.organizations[0]);

      const orgExternalId = searchAttributes.id;
      const orgMs = await methods.getOrganizationByMsDetail(orgExternalId);
      setters.setOrganizationByMs(orgMs);

      const dataMarketingMs = await methods.getMarketingByMs(orgExternalId);
      if(dataMarketingMs != null){
        const dataProgramIdMs = await methods.getProgramIdByMs(dataMarketingMs.programId);
        setters.setMarketingByMs(dataProgramIdMs);
      }else{
        setters.setMarketingByMs([]);
      }

      const OrdersMs = await methods.getOrdersByMicroService(orgExternalId);
      OrdersMs[0].orders.sort(function (x, y) {
        let a = new Date(x.placementDate),
            b = new Date(y.placementDate);
        return b- a;
      });
      setters.setOrdersMs(OrdersMs[0].orders);
      setters.setOrderMsInMemory(OrdersMs[0].orders);
      if (getters.getIsNewCreateOrgByMs()) setters.setIsNewCreateOrgByMs();
      if (getters.getIsErrorConsultAccount()) setters.setIsErrorConsultAccount();
    } 
    else {
      let accountMS =  await methods.getOrganizationByMsDetail(searchAttributes.id);
      if(accountMS.length > 0){
        
        let newOrgZenDesk = await methods.insertNewOrganizationZenDesk(accountMS);
        setters.setIsNewCreateOrgByMs();
        setters.setCreateNewOrgByMs('createNewOrgByMs');

        setters.setMultipleOrganizationUsers(newOrgZenDesk.organization.id);
        const orgExternalId = searchAttributes.id;
        setters.setOrganizationByMs(accountMS);
        let OrdersMs = await methods.getOrdersByMicroService(orgExternalId);
        OrdersMs[0].orders.sort(function (x, y) {
            let a = new Date(x.placementDate),
                b = new Date(y.placementDate);
            return b- a;
          });
        setters.setOrdersMs(OrdersMs[0].orders);
        setters.setOrderMsInMemory(OrdersMs[0].orders);
        const dataMarketingMs = await methods.getMarketingByMs(orgExternalId);
        if(dataMarketingMs != null){
          const dataProgramIdMs = await methods.getProgramIdByMs(dataMarketingMs.programId);
          setters.setMarketingByMs(dataProgramIdMs);
        }else{
          setters.setMarketingByMs([]);
      }
        const orgDetails = await this.getOrganizationByExternalId(orgExternalId);
        setters.setProfile(orgDetails.organizations[0]);
        if (getters.getIsErrorConsultAccount()) setters.setIsErrorConsultAccount();
      }else{
        if (getters.getIsErrorConsultAccount()) setters.setIsNewCreateOrgByMs();
        if (getters.getIsErrorConsultAccount()) setters.setIsErrorConsultAccount();
        methods.resetValuesAfterNotFound();
        setters.setIsErrorConsultAccount();
      }
    }
    setters.setLoader();
  },

  checkSameOrganization(requesterOrganizations, currentOrganizationId) {
    let isSameOrganization = requesterOrganizations.map((organization) => {
      if (organization.id === currentOrganizationId) return true;
    });
    if (isSameOrganization[0]) {
      setters.setIsDisplayingSearchResults();
    } else {
      state.isDisplayingSearchResults = false;
    }
  },
  getRequesterEmailAndPhone(requesterIdentities) {
    setters.setRequesterDetails({
      email: requesterIdentities.find(({ type }) => type === 'email') === undefined
        ? ''
        : requesterIdentities.find(({ type }) => type === 'email').value,
      phone: requesterIdentities.find(({ type }) => type === 'phone_number') === undefined
        ? ''
        : requesterIdentities.find(({ type }) => type === 'phone_number').value
    });
  },

  async getOrganizationByExternalId(externalId) {
    try {
      const requestBody = {
        url: `/api/v2/organizations/show_many.json?external_ids=${externalId}`,
        method: 'GET'
      }
      return await ZDClient.request(requestBody);
    } catch (error) {
      return null;
    }
  },

  async insertNewOrganizationZenDesk(accountMS) {
    try {
      const nameOrg = accountMS[0].accountId + ' - '+accountMS[0].name;
      const requestBody = {
        url: '/api/v2/organizations/create_or_update.json',
        contentType:'application/json',
        type: 'POST',
        data: JSON.stringify({organization: {external_id: accountMS[0].accountId, name: nameOrg,
          organization_fields:{poc_name :accountMS[0].name,poc_id : accountMS[0].accountId,
          room : accountMS[0].erpSalesCenter, ddc : accountMS[0].deliveryCenterId}} })
      }
      return await ZDClient.request(requestBody);
    } catch (error) {
      return null;
    }
  },

  async getOrganizationByMicroService(externalId) {
    try {
      const URL_INITIAL = ZDClient.app.settings.microserviceURL;
      const AUTHORIZATION_TOKEN =  state.acessToken;
      const REQUEST_TRACE_ID_MS = ZDClient.app.settings.microserviceRequestTraceId;
      const COUNTRY = ZDClient.app.settings.microserviceCountry;
      const ACCOUNT_SERVICE = ZDClient.app.settings.microserviceAccount;

      const requestBody = {
        url: URL_INITIAL + ACCOUNT_SERVICE + externalId,
        type: 'GET',
        headers: {
          'Authorization':AUTHORIZATION_TOKEN,
          'requestTraceId':REQUEST_TRACE_ID_MS,
          'country':COUNTRY
       },
       dataType: 'json'
      }
      return await ZDClient.request(requestBody);
    } catch (error) {
      return null;
    }
  },

  async getOrdersByMicroService(externalId) {
    try {
      const URL_INITIAL = ZDClient.app.settings.microserviceURL;
      const AUTHORIZATION_TOKEN =  state.acessToken;
      const REQUEST_TRACE_ID_MS = ZDClient.app.settings.microserviceRequestTraceId;
      const COUNTRY = ZDClient.app.settings.microserviceCountry;
      const ORDER_SERVICE = ZDClient.app.settings.microserviceOrders;

      const requestBody = {
        url:  URL_INITIAL + ORDER_SERVICE + externalId,
        type: 'GET',
        headers: {
          'Authorization':AUTHORIZATION_TOKEN,
          'requestTraceId':REQUEST_TRACE_ID_MS,
          'country':COUNTRY
       },
       dataType: 'json'
      }
      return await ZDClient.request(requestBody);
    } catch (error) {
      return null;
    }
  },

  async getMarketingByMs(externalId) {
    try {
      const URL_INITIAL = ZDClient.app.settings.microserviceURL;
      const AUTHORIZATION_TOKEN =  state.acessToken;
      const REQUEST_TRACE_ID_MS = ZDClient.app.settings.microserviceRequestTraceId;
      const COUNTRY = ZDClient.app.settings.microserviceCountry;
      const REWARDS_SERVICE = ZDClient.app.settings.microserviceRewards;

      const requestBody = {
        url: URL_INITIAL + REWARDS_SERVICE + externalId,
        type: "GET",
        headers: {
          'Authorization':AUTHORIZATION_TOKEN,
          'requestTraceId':REQUEST_TRACE_ID_MS,
          'country':COUNTRY
       },
        dataType: "json",
      };
      return await ZDClient.request(requestBody);
    } catch (error) {
      return null;
    }
  },

  async getProgramIdByMs(programId) {
    try {
      const URL_INITIAL = ZDClient.app.settings.microserviceURL;
      const AUTHORIZATION_TOKEN =  state.acessToken;
      const REQUEST_TRACE_ID_MS = ZDClient.app.settings.microserviceRequestTraceId;
      const COUNTRY = ZDClient.app.settings.microserviceCountry;
      const PROGRAM_ID_SERVICE = ZDClient.app.settings.microserviceProgramId;
      const PROJECTION_DEFAULT = '?projection=DEFAULT';
      
      const requestBody = {
        url: URL_INITIAL + PROGRAM_ID_SERVICE + programId + PROJECTION_DEFAULT,
        type: "GET",
        headers: {
          'Authorization':AUTHORIZATION_TOKEN,
          'requestTraceId':REQUEST_TRACE_ID_MS,
          'country':COUNTRY
       },
        dataType: "json",
      };
      return await ZDClient.request(requestBody);
    } catch (error) {
      return null;
    }
  },

  async getUsersFromOrganizations(organizationId) {
    try {
      const requestBody = {
        url: `/api/v2/organizations/${organizationId}/users.json`,
        method: 'GET'
      }
      return await ZDClient.request(requestBody);
    } catch (error) {
      return null;
    }
  },

  async getDataFromOrganizations(organizationId) {
    try {
      const requestBody = {
        url: `/api/v2/organizations/${organizationId}`,
        method: 'GET'
      }
      return await ZDClient.request(requestBody);
    } catch (error) {
      return null;
    }
  },

  resetValuesAfterNotFound() {
    setters.setProfile(null);
    setters.setOrdersMs([]);
    setters.setErrorMessage('notFound');
    setters.setIsError();
    setters.setUsersFromOrganizations([]);
    if (state.isDisplayingSearchResults) setters.setIsDisplayingSearchResults();
    setters.setTemporalOrganizationId(0);
    setters.setRequesterDetails(null);
  },

  async getAccountRoles() {
    try {
      const requestBody = {
        url: '/api/v2/custom_roles.json',
        method: 'GET'
      }
      return await ZDClient.request(requestBody);
    } catch (error) {
      return null;
    }
  },
  async createNewUser(newUserData) {
    try {
      const requestBody = {
        url: '/api/v2/users.json',
        data: JSON.stringify(newUserData),
        method: 'POST',
        contentType: 'application/json'
      }
      return await ZDClient.request(requestBody);
    } catch (error) {
      return null;
    }
  },

  editRolePermission(index, roleId) {
    const indexOfElement = state.rolesPermissions[index].groups.indexOf(roleId);
    indexOfElement === -1
      ? state.rolesPermissions[index].groups.push(roleId)
      : state.rolesPermissions[index].groups.splice(indexOfElement, 1);
  },

  async editRolesPermissions() {
    let installationId = ZDClient.app.isProduction ? ZDClient.app.settings.installationId : 0
    let requestBody = {
      url: `/api/v2/apps/installations/${installationId}.json`,
      method: 'PUT',
      type: 'PUT',
      contentType: 'application/json',
      data: JSON.stringify({
        settings: {
          rolePermissions: JSON.stringify(state.rolesPermissions)
        }
      })
    };
    try {
      const appSettings = await ZDClient.request(requestBody);
      if (appSettings) {
        this.sendNotification(appSettings);
      }
      return appSettings;
    } catch (error) {
      return null;
    }
  },

  sendNotification(appSettings) {
    let settingsToSend = appSettings;
    settingsToSend['roleUpdated'] = state.selectedRoleId;
    settingsToSend.settings['installationId'] = ZDClient.app.settings.installationId;
    settingsToSend.settings['appId'] = ZDClient.app.settings.appId;
    let requestBody = {
      url: `/api/v2/apps/notify.json`,
      method: 'POST',
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify({
        event: 'updateSettings',
        app_id: ZDClient.app.isProduction ? ZDClient.app.settings.appId : 0,
        body: settingsToSend
      })
    };
    ZDClient.request(requestBody);
  },

  async getTicketFormFields() {
    ZDClient.getCurrentFormFields()
      .then((formFields) => {
        let fieldMapping = JSON.parse(ZDClient.app.settings.ticketFormFields);
        formFields.ticketFields.map((field) => {
          let ticketFieldName = field.name.split('_');
          let ticketFieldId = ticketFieldName[2];
          let mappedFieldValue = fieldMapping[ticketFieldId];
          
          if (ticketFieldName[0] === 'custom' && mappedFieldValue) {
            ZDClient.getTicketFieldValue(ticketFieldId).then((ticketField) => {
              let currentTicketFieldValue = ticketField['ticket.customField:'+field.name];
              if (!currentTicketFieldValue) {
                let attributeValue = methods.findValue(state.userOrdersMs, mappedFieldValue);
                if (!attributeValue) attributeValue = methods.findValue(state.userProfile, mappedFieldValue);
                ZDClient.setTicketFieldValue(ticketFieldId, attributeValue || '');
              }
            });
          }
        });
      });
  },

  findValue(object, value) {
    for (let key in object) {
      if (typeof object[key] === 'object') {
        const match = methods.findValue(object[key], value);
        if (match) {
          return match;
        }
      }
      if (key === value) {
        return object[key];
      }
    }
    return false;
  },

  async setOrdersUrl() {
    const currentLocation = await ZDClient.getCurrentLocation();
    const currentUserDetails = await ZDClient.currentUserDetails();
    let urlQueryParameters = 'poc_id=' + state.userProfile.external_id
      + '&zendesk_agent_id=' + currentUserDetails.currentUser.id
      + '&zendesk_agent_email=' + currentUserDetails.currentUser.email
      + '&zendesk_agent_name=' + currentUserDetails.currentUser.name;
    
    if(currentLocation !== "organization_sidebar") {
      const ticketRequesterDetails = await ZDClient.getTicketRequester();
      urlQueryParameters += '&zendesk_enduser_id=' + ticketRequesterDetails['ticket.requester']['id']
        + '&userId=' + ticketRequesterDetails['ticket.requester']['externalId'];
    }
    
    if(currentLocation === "ticket_sidebar") {
      const ticketId = await ZDClient.getTicketId();
      urlQueryParameters += '&zendesk_ticket_id=' + ticketId['ticket.id'];
    }
    
    setters.setTakeOrdersUrl(ZDClient.app.settings.ordersUrl + "?" + encodeURIComponent(urlQueryParameters));
  },


  async verifyAcessToken(app360View){
    state.acessToken = app360View[0].settings.microserviceToken;
    const timeZone = await ZDClient.getCurrentAccountTimeZone();
    let requestDate = new Date();
    let currentDateTZ = new Date(requestDate.toLocaleString('en-US', { timeZone: timeZone["currentAccount.timeZone"].ianaName }));
    
    let currentDateTZFormatted = `${currentDateTZ.getFullYear()}-${utils.removeGMTAndDefaultHour(currentDateTZ.getMonth() + 1)}-${utils.removeGMTAndDefaultHour(currentDateTZ.getDate())}T${utils.removeGMTAndDefaultHour(currentDateTZ.getHours())}:${utils.removeGMTAndDefaultHour(currentDateTZ.getMinutes())}:${utils.removeGMTAndDefaultHour(currentDateTZ.getSeconds())}`;
    let expirationDateToken = `${currentDateTZ.getFullYear()}-${utils.removeGMTAndDefaultHour(currentDateTZ.getMonth() + 1)}-${utils.removeGMTAndDefaultHour(currentDateTZ.getDate())}T${utils.removeGMTAndDefaultHour(currentDateTZ.getHours()+1)}:${utils.removeGMTAndDefaultHour(currentDateTZ.getMinutes())}:${utils.removeGMTAndDefaultHour(currentDateTZ.getSeconds())}`;

    let tokeAcessBees
    if(app360View[0].settings.tokenExpirationDate === null || currentDateTZFormatted >= app360View[0].settings.tokenExpirationDate){
      tokeAcessBees = await methods.getTokenBees();
      await methods.updateTokenApp(ZDClient.app.settings.installationId,tokeAcessBees,expirationDateToken);
      console.log('Integration to get the token performed!!!'); 
    }else{
      console.log('Integration to get the token not done!!!');      
    }

    if(tokeAcessBees){
      state.acessToken = tokeAcessBees.token_type + ' ' + tokeAcessBees.access_token;
    }
  },
      
  async updateTokenApp(AppId,tokeAcessBees,expirationDateToken) {
    try {

    let tokenAcess = tokeAcessBees.token_type + " " +tokeAcessBees.access_token;
    state.acessToken = tokenAcess;    
    let dataString = `{"settings": {"tokenExpirationDate": "${expirationDateToken}", "microserviceToken": "${tokenAcess}"}}`;

    const requestBody = {
      url: `/api/v2/apps/installations/${AppId}.json`,
      method: 'PUT',
      type: 'PUT',
      contentType: 'application/json',
      data: dataString
    }
    return await ZDClient.request(requestBody);
  } catch (error) {
    return null;
  }
},

async getTokenBees() {
  try {

    var param = {
      grant_type: ZDClient.app.settings.microserviceGrantType,
      client_id : ZDClient.app.settings.microserviceClientId,
      client_secret : ZDClient.app.settings.microserviceClientSecret,
      scope : ZDClient.app.settings.microserviceScope
    };
    let urlAzure = ZDClient.app.settings.acessTokenURL;
    
    const requestBody = {
      url: urlAzure,
      method: 'POST',
      type: 'POST',
      contentType: "application/x-www-form-urlencoded",
      data : param
  }

  return await ZDClient.request(requestBody);

  }catch (error)
  {
    return null;
   }
},
async getAppsInstall() {
  try {
    const requestBody = {
      url: '/api/v2/apps/installations.json',
      method: 'GET'
    }
    return await ZDClient.request(requestBody);
  } catch (error) {
    return null;
  }
},

};